package Tests;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import org.junit.jupiter.api.Test;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di testare operazioni base di creazione, modifica, eliminazione e ricerca per ogni gestore delle entità.
 */
class TestCasesBase {

	@Test
	void testPosizioneCreate() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    assertEquals(pos.getCitta(), "milano");
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
	
	@Test
	void testPosizioneUpdate() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    cp.updatePosizione(id, "libertà 19","Roma", 24245, "Lazio", "RM");
		    pos = cp.findPosizione(id).get(0);
		    assertEquals(pos.getCitta(), "Roma");
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
	
	@Test
	void testPosizioneDelete() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
			int numeroPosizioni= cp.findPosizione().size();
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
			cp.deletePosizione(id);
			List<Posizione> list =cp.findPosizione();
			assertEquals(list.size(), numeroPosizioni);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
	
	@Test
	void testFindProduttore() {
		try {
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
		    cp.deletePosizione(idpos);
		    List<Posizione> listpos =cp.findPosizione(idpos);
		    assertTrue(listpos.isEmpty());    
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
	
	@Test
	void testVenditoreCreate() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
			int numeroVenditori = cv.findVenditore().size();
		    int id = cv.createVenditore("esselunga","intensivo");
		    int id2= cv.createVenditore("carrefour","intensivo");
		    Venditore ven = cv.findVenditore(id).get(0);
		    assertEquals(ven.getNome(), "esselunga");
		    cv.updateVenditore(id,"newEsselunga","intesivo");
		    ven = cv.findVenditore(id).get(0);
		    assertEquals(ven.getNome(), "newEsselunga");
			List<Venditore> list = cv.findVenditore();
			assertEquals(list.size(),numeroVenditori+2);
			cv.deleteVenditore(id);
		    cv.deleteVenditore(id2);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testVenditoreUpdate() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    int id = cv.createVenditore("esselunga","intensivo");
		    Venditore ven = cv.findVenditore(id).get(0);
		    assertEquals(ven.getNome(), "esselunga");
		    cv.updateVenditore(id,"newEsselunga", "intensivo");
		    ven = cv.findVenditore(id).get(0);
		    assertEquals(ven.getNome(), "newEsselunga");
		    cv.deleteVenditore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testVenditoreDelete() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
			int numeroVenditori = cv.findVenditore().size();
		    int id = cv.createVenditore("esselunga","intensivo");
		    int id2= cv.createVenditore("carrefour","intensivo");
		    cv.deleteVenditore(id);
		    cv.deleteVenditore(id2);
			List<Venditore> list =cv.findVenditore();
			assertEquals(list.size(),numeroVenditori);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testFindVenditore() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
			int id = cv.createVenditore("esselunga","intensivo");
			cv.deleteVenditore(id);
			List<Venditore> list =cv.findVenditore(id);
		    assertTrue(list.isEmpty());    
		}
		catch(NomeNullException e){
			fail();
		}	
	}
	
	@Test
	void testFormaggioCreate() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getNome(), "parmigiano");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFormaggioUpdate() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getNome(), "parmigiano");
		    cf.updateFormaggio(id, 456.0, "parmigiano", 12, "vaccino", "cagliato");
		    f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getPrezzo(), 456.0);
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFormaggioDelete() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
			int numeroFormaggi = cf.findFormaggio().size();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    cf.delete(id);
		    List<Formaggio> list =cf.findFormaggio();
			assertEquals(list.size(), numeroFormaggi);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFindFormaggio() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    cf.delete(id);
		    List<Formaggio> list =cf.findFormaggio(id);
		    assertTrue(list.isEmpty());    
		}
		catch(NomeNullException | PrezzoNullNegativeException e){
			fail();
		}	
	}
	
	@Test
	void testFruttoCrete() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			
			Frutto f = cf.findFrutto(id).get(0);
		    assertEquals(f.getNome(), "banana");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}			
	}
	
	@Test
	void testFruttoUpdate() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findFrutto(id).get(0);
		    assertEquals(f.getNome(), "banana");
		    cf.updateFrutto(id,32.0,"banana", "inverno", "giallo", "cichita");
		    f = cf.findFrutto(id).get(0);
		    assertEquals(f.getPrezzo(), 32.0);
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFruttoDelete() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int numeroFrutti = cf.findFrutto().size();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
		    cf.delete(id);
		    List<Frutto> list =cf.findFrutto();
			assertEquals(list.size(), numeroFrutti);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}
				
	}
	
	@Test
	void testFruttoFind() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
		    cf.delete(id);
		    List<Frutto> list =cf.findFrutto(id);
			assertTrue(list.isEmpty());
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}
				
	}
	
	
	  
    
	
	@Test
	void testProduttoreCreate() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    Produttore pro = cv.findProduttore(id).get(0);
		    assertEquals(pro.getNome(), "ciccio");
		    cv.deleteProduttore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testProduttoreUpdate() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    Produttore pr = cv.findProduttore(id).get(0);
		    cv.updateProduttore(id,"pluto", 23124, "intensiva");
		    pr = cv.findProduttore(id).get(0);
		    assertEquals(pr.getNome(), "pluto");
		    cv.deleteProduttore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testProduttoreDelete() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
			int numeroVenditori = cv.findProduttore().size();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    int id2= cv.createProduttore("pluto", 23124, "intensiva");
		    cv.deleteProduttore(id);
		    cv.deleteProduttore(id2);
			List<Produttore> list =cv.findProduttore();
			assertEquals(list.size(),numeroVenditori);
		}
		catch(NomeNullException e){
			fail();
		}
	}	
}
