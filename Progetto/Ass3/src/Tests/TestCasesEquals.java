package Tests;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import Beans.Posizione;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;

import org.junit.jupiter.api.Test;
/**
 * Classe di test, che permette di testare per ogni entità i metodi equals.
 */
class TestCasesEquals {

	@Test
	void testEqualsFormaggio() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int f1 = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    assertTrue(cf.findFormaggio(f1).get(0).equals(cf.findFormaggio(f1).get(0)));
		    cf.delete(f1);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}
	}
	
	@Test
	void testNotEqualsFormaggio() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int f1 = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    int f2 = cf.createFormaggio(10.0, "zola", 32, "vaccino", "noncagliato");
		    assertFalse(cf.findFormaggio(f1).get(0).equals(cf.findFormaggio(f2).get(0)));
		    cf.delete(f1);
		    cf.delete(f2);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}
	}
	
	@Test
	void testEqualsFrutto() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int f1 = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
		    assertTrue(cf.findFrutto(f1).get(0).equals(cf.findFrutto(f1).get(0)));
		    cf.delete(f1);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}			
	}
	
	@Test
	void testNotEqualsFrutto() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int f1 = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			int f2 = cf.createFrutto(10.0, "pera", "estate", "verde", "cichita");
		    assertFalse(cf.findFrutto(f1).get(0).equals(cf.findFrutto(f2).get(0)));
		    cf.delete(f1);
		    cf.delete(f2);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}			
	}
	
	@Test
	void testEqualsProduttore() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
			int id = cv.createProduttore("ciccio", 12345, "intensiva",pos);
			assertTrue(cv.findProduttore(id).get(0).equals(cv.findProduttore(id).get(0)));
			cv.deleteProduttore(id);
	      
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	 }
	
	@Test
	void testNotEqualsProduttore() {
		try {
			ControllerProduttore cp = new ControllerProduttore();
		    int p1 = cp.createProduttore("ciccio", 12345, "intensiva");
		    int p2 = cp.createProduttore("enzo", 54321, "privata");
		    assertFalse(cp.findProduttore(p1).get(0).equals(cp.findProduttore(p2).get(0)));
		    cp.deleteProduttore(p1);
		    cp.deleteProduttore(p2);
		    
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testEqualsPosizione() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int p1 = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    assertTrue(cp.findPosizione(p1).get(0).equals(cp.findPosizione(p1).get(0)));
		    cp.deletePosizione(p1);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testNotEqualsPosizione() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int p1 = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    int p2 = cp.createPosizione("Giotto 3","Torino", 24245, "Lombardia", "MI");
		    assertFalse(cp.findPosizione(p1).get(0).equals(cp.findPosizione(p2).get(0)));		   	
		    cp.deletePosizione(p1);
		    cp.deletePosizione(p2);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testEqualsVenditore() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    int v1 = cv.createVenditore("esselunga","intensivo");
		    //socio
		    int v3= cv.createVenditore("lidl","intensivo");
		    cv.addSocio(v1, cv.findVenditore(v3).get(0));
		    //posizioni
		    ControllerPosizione cp = new ControllerPosizione(); 
		    int p1 = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    cv.addPosizione(v1, cp.findPosizione(p1).get(0));    
		    assertTrue(cv.findVenditore(v1).get(0).equals(cv.findVenditore(v1).get(0)));
			cv.deleteVenditore(v1);
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
	
	@Test
	void testNotEqualsVenditore() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    int v1 = cv.createVenditore("esselunga","intensivo");
		    int v2= cv.createVenditore("esselunga","intensivo");
		    //socio
		    int v3= cv.createVenditore("lidl","intensivo");
		    int v4= cv.createVenditore("spark","intensivo");
		    cv.addSocio(v1, cv.findVenditore(v3).get(0));
		    cv.addSocio(v2, cv.findVenditore(v4).get(0));
		    //posizioni
		    ControllerPosizione cp = new ControllerPosizione(); 
		    int p1 = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    cv.addPosizione(v1, cp.findPosizione(p1).get(0));
		    cv.addPosizione(v2, cp.findPosizione(p1).get(0));		    
		    assertFalse(cv.findVenditore(v1).get(0).equals(cv.findVenditore(v2).get(0)));
			cv.deleteVenditore(v1);
			cv.deleteVenditore(v2);
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
}
