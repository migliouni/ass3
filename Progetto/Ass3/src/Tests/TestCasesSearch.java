package Tests;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di verificare i metodi di ricerca per ogni entità disponibile.
 */
class TestCasesSearch {
	@Test
	void testFormaggioSearchByName() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findByName("parmigiano").get(0);
		    assertEquals(f.getNome(), "parmigiano");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFormaggioSearchByPrice() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findByUpperPrice(40).get(0);
		    assertEquals(f.getNome(), "parmigiano");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFruttoSearchByName() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findByName("banana").get(0);
		    assertEquals(f.getNome(), "banana");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}			
	}
	
	@Test
	void testFruttoSearchByPrice() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findByUpperPrice(40).get(0);
		    assertEquals(f.getNome(), "banana");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}			
	}
	
	@Test
	void testProduttoreSearchByName() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    Produttore pro = cv.findByName("ciccio").get(0);
		    assertEquals(pro.getNome(), "ciccio");
		    cv.deleteProduttore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testVenditoreSearchByName() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    int id = cv.createVenditore("esselunga","intensivo");
		    Venditore ven = cv.findByName("esselunga").get(0);
		    assertEquals(ven.getNome(), "esselunga");
			cv.deleteVenditore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testPosizioneSearchByName() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findByName("milano").get(0);
		    assertEquals(pos.getCitta(), "milano");
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
}
