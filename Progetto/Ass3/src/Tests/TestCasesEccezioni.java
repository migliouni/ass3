package Tests;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import org.junit.jupiter.api.Test;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di testare l'effettivo lancio di eccezioni personalizzate.
 */
class TestCasesEccezioni {
	
	@Test
	void testPosizioneIndirizzo() {
		ControllerPosizione cp = new ControllerPosizione(); 
		Throwable exception = assertThrows(IndirizzoNotValidException.class, () -> cp.createPosizione(null,"monza", 2425, "Lombardia", "MI")); 
		assertEquals("L'indirizzo non può essere nullo", exception.getMessage());	
	}
	
	
	@Test
	void testPosizione() {
		ControllerPosizione cp = new ControllerPosizione(); 
		Throwable exception = assertThrows(PosizioneNotValidException.class, () -> cp.createPosizione("libertà 25",null, 2425, "Lombardia", "MI")); 
		assertEquals("La posizione non può avere citta null!", exception.getMessage());	
	}
	
	@Test
	void testPosizioneProvincia() {
		ControllerPosizione cp = new ControllerPosizione(); 
		Throwable exception = assertThrows(ProvinciaNotValidException.class, () -> cp.createPosizione("libertà 25","monza", 24245, "Lombardia", "MIn")); 
		assertEquals("La provincia deve essere composta da due lettere!", exception.getMessage());
	}
	
	@Test
	void testPosizioneCap() {
		ControllerPosizione cp = new ControllerPosizione(); 
		Throwable exception = assertThrows(CapNotValidException.class, () -> cp.createPosizione("libertà 25","monza", 2425, "Lombardia", "MI")); 
		assertEquals("Il cap deve essere composto da 5 numeri interi!", exception.getMessage());
	}
	
	
	@Test
	void testVenditoreEccezione() {
		ControllerVenditore cv = new ControllerVenditore();
		Throwable exception = assertThrows(NomeNullException.class, () -> cv.createVenditore(null,"intensivo")); 
		assertEquals("Il nome non può assumere valore null!", exception.getMessage()); 
	
	}
	
	@Test
	void testFormaggioEccezioneNeg() {
		ControllerFormaggio cf = new ControllerFormaggio();
		Throwable exception = assertThrows(PrezzoNullNegativeException.class, () -> cf.createFormaggio(-2, "parmigiano", 12, "vaccino", "cagliato")); 
		assertEquals("Il prezzo di un prodotto non può essere vuoto o negativo!", exception.getMessage());
	}
	
	@Test
	void testFormaggioEccezioneNull() {
		ControllerFormaggio cf = new ControllerFormaggio();
		Throwable exception2 = assertThrows(NomeNullException.class, () -> cf.createFormaggio(20, null, 12, "vaccino", "cagliato")); 
		assertEquals("Il nome non può assumere valore null!", exception2.getMessage()); 
	}
	
	
	@Test
	void testFruttoEccezioneNeg() {
		ControllerFrutto cf = new ControllerFrutto();
		Throwable exception = assertThrows(PrezzoNullNegativeException.class, () -> cf.createFrutto(-30.0, "banana", "inverno", "giallo", "cichita")); 
		assertEquals("Il prezzo di un prodotto non può essere vuoto o negativo!", exception.getMessage());
			 
	}
	
	@Test
	void testFruttoEccezioneNull() {
		ControllerFrutto cf = new ControllerFrutto();
		Throwable exception2 = assertThrows(NomeNullException.class, () -> cf.createFrutto(30.0, null, "inverno", "giallo", "cichita")); 
		assertEquals("Il nome non può assumere valore null!", exception2.getMessage()); 
		}
	
	
	@Test
	void testPosizioneUpdateProvincia() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
			int id = cp.createPosizione("libertà 15", "milano", 2425, "Lombardia", "MI");
			Throwable exception = assertThrows(ProvinciaNotValidException.class, () -> cp.updateProvincia(id,"MIn")); 
			assertEquals("La provincia deve essere composta da due lettere!", exception.getMessage());
			cp.deletePosizione(id);
			} catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException| IndirizzoNotValidException e) {
			}
	}
	
	@Test
	void testPosizioneUpdateCap() {
		ControllerPosizione cp = new ControllerPosizione(); 
		try {
			int id = cp.createPosizione("libertà 15", "milano", 2425, "Lombardia", "MI");
			Throwable exception = assertThrows(CapNotValidException.class, () -> cp.updateCap(id,2425)); 
			assertEquals("Il cap deve essere composto da 5 numeri interi!", exception.getMessage());
			cp.deletePosizione(id);
			} 
		catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException
				| IndirizzoNotValidException e) {}
	}
	
	@Test
	void testPosizioneUpdateCitta() {
		ControllerPosizione cp = new ControllerPosizione(); 
		try {
			int id = cp.createPosizione("libertà 15", "milano", 2425, "Lombardia", "MI");
			Throwable exception = assertThrows(PosizioneNotValidException.class, () -> cp.updateCitta(id, null)); 
			assertEquals("La posizione non può avere citta null!", exception.getMessage());
			cp.deletePosizione(id);
		} catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException
				| IndirizzoNotValidException e) {
			
			 
		}
	}
	
	@Test
	void testPosizioneUpdateIndirizzo() {
		ControllerPosizione cp = new ControllerPosizione(); 
		try {
			int id = cp.createPosizione("libertà 15", "milano", 2425, "Lombardia", "MI");
			Throwable exception = assertThrows(IndirizzoNotValidException.class, () -> cp.updateIndirizzo(id, null)); 
			assertEquals("L'indirizzo non può essere nullo", exception.getMessage());
			cp.deletePosizione(id);
		} catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException
				| IndirizzoNotValidException e) {
		}
	}
	
	@Test
	void testFormaggioEccezioneUpdatePrezzoNegative() {
		ControllerFormaggio cf = new ControllerFormaggio();
		try {
			int id = cf.createFormaggio(10, "parmigiano", 12, "vaccino", "cagliato");
			Throwable exception = assertThrows(PrezzoNullNegativeException.class, () -> cf.updatePrezzo(id,-2)); 
			assertEquals("Il prezzo di un prodotto non può essere vuoto o negativo!", exception.getMessage());
			cf.delete(id);
		} catch (PrezzoNullNegativeException | NomeNullException e) {
				 
		}
		
	}
	
	@Test
	void testFormaggioEccezioneUpdateNome() {
		ControllerFormaggio cf = new ControllerFormaggio();
		try {
			int id =cf.createFormaggio(10, "parmigiano", 12, "vaccino", "cagliato");
			cf.updateNome(id,null);
			Throwable exception2 = assertThrows(NomeNullException.class, () -> cf.updateNome(id,null)); 
			assertEquals("Il nome non può assumere valore null!", exception2.getMessage()); 
			cf.delete(id);
		} catch (PrezzoNullNegativeException | NomeNullException e) {
			 
		}
		
	}
	
	@Test
	void testFruttoEccezioneUpdateNull() {
		ControllerFrutto cf = new ControllerFrutto();
		try {
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Throwable exception2 = assertThrows(NomeNullException.class, () -> cf.updateNome(id,null)); 
			assertEquals("Il nome non può assumere valore null!", exception2.getMessage()); 
			cf.delete(id);
		} catch (PrezzoNullNegativeException | NomeNullException e) {
			
		}
	}
	
	@Test
	void testFruttoEccezioneUpdatePrezzo() {
		ControllerFrutto cf = new ControllerFrutto();
		try {
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Throwable exception = assertThrows(PrezzoNullNegativeException.class, () -> cf.updatePrezzo(id, -30.0)); 
			assertEquals("Il prezzo di un prodotto non può essere vuoto o negativo!", exception.getMessage());
			cf.delete(id);
		} catch (PrezzoNullNegativeException | NomeNullException e) {
				 
		}
			
		}					
	}
	
	
	

	


