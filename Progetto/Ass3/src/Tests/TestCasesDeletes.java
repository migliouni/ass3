package Tests;
import static org.junit.Assert.fail; 
import static org.junit.jupiter.api.Assertions.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di testare operazioni di eliminazione di elementi con più entità collegate.
 */
class TestCasesDeletes {
	
	@Test
	void testDeleteProduttore() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
		    int id = cv.createProduttore("ciccio", 12345, "intensiva",pos);   
		    cv.deleteProduttore(id);
		    List<Produttore> list =cv.findProduttore(id);
		    List<Posizione> listpos =cp.findPosizione(idpos);
		    assertTrue(list.isEmpty());
		    assertFalse(listpos.isEmpty());
		    cp.deletePosizione(idpos); 
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
	
	@Test
	void testDeleteVenditore() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
			Set<Posizione> posizioni = new HashSet<Posizione>();
			posizioni.add(pos);
		    int id = cv.createVenditore("esselunga", "intensivo", posizioni);
		    cv.deleteVenditore(id);	    
		    List<Venditore> list =cv.findVenditore(id);
		    List<Posizione> listpos =cp.findPosizione(idpos);
		    assertTrue(list.isEmpty());
		    assertFalse(listpos.isEmpty());
		    cp.deletePosizione(idpos);
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
		
	}
	
	@Test
	void testDeletePosizione() {
		try {
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
		    cp.deletePosizione(idpos);	   
		    List<Posizione> list =cp.findPosizione(idpos);
		    assertTrue(list.isEmpty());
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
	
	@Test
	void testdeleteFormaggio() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
			ControllerVenditore cv = new ControllerVenditore();
			ControllerProduttore cp = new ControllerProduttore();
			int idven = cv.createVenditore("esselunga", "intensivo");
			int idprod = cp.createProduttore("ciccio", 12345, "intensiva");
			Set<Venditore> ven = new HashSet<Venditore>();
			ven.add(cv.findVenditore(idven).get(0));
			Set<Produttore> prod = new HashSet<Produttore>();
			prod.add(cp.findProduttore(idprod).get(0));			
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato", ven, prod);
		    cf.delete(id);    
		    List<Formaggio> formaggi =cf.findFormaggio(id);
		    List<Venditore> venditori =cv.findVenditore(idven);
		    List<Produttore> posizioni = cp.findProduttore(idprod);		    
		    assertTrue(formaggi.isEmpty());
		    assertFalse(venditori.isEmpty());
		    assertFalse(posizioni.isEmpty());		   
		    cv.deleteVenditore(idven);
		    cp.deleteProduttore(idprod);		    
		}
		catch(NomeNullException | PrezzoNullNegativeException e){
			fail();
		}
	}
	
	@Test
	void testdeleteFrutto() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			ControllerVenditore cv = new ControllerVenditore();
			ControllerProduttore cp = new ControllerProduttore();
			int idven = cv.createVenditore("esselunga", "intensivo");
			int idprod = cp.createProduttore("ciccio", 12345, "intensiva");
			Set<Venditore> ven = new HashSet<Venditore>();
			ven.add(cv.findVenditore(idven).get(0));			
			Set<Produttore> prod = new HashSet<Produttore>();
			prod.add(cp.findProduttore(idprod).get(0));			
		    int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", ven, prod);		    		    
		    cf.delete(id);    		    
		    List<Frutto> formaggi = cf.findFrutto(id);
		    List<Venditore> venditori =cv.findVenditore(idven);
		    List<Produttore> posizioni = cp.findProduttore(idprod);		    
		    assertTrue(formaggi.isEmpty());
		    assertFalse(venditori.isEmpty());
		    assertFalse(posizioni.isEmpty());
		    cv.deleteVenditore(idven);
		    cp.deleteProduttore(idprod);		    
		}
		catch(NomeNullException | PrezzoNullNegativeException e){
			fail();
		}
		
	}
	
	//casi di test con rimozioni di oggetti in relazioni eliminando l'oggetto riferito, es eliminando la posizione dove risiede un venditore
	@Test
	void testDeleteProduttoreNonOrdinate() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
		    int id = cv.createProduttore("ciccio", 12345, "intensiva",pos);   
		    cv.deleteProduttore(id);
		    cp.deletePosizione(idpos);
		    List<Produttore> list =cv.findProduttore(id);
		    List<Posizione> listpos =cp.findPosizione(idpos);
		    assertTrue(list.isEmpty());
		    assertTrue(listpos.isEmpty());  
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}	
	}
	
	@Test
	void testDeleteVenditoreNonOrdinate() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
			Set<Posizione> posizioni = new HashSet<Posizione>();
			posizioni.add(pos);
		    int id = cv.createVenditore("esselunga", "intensivo", posizioni);
		    cp.deletePosizione(idpos);
		    cv.deleteVenditore(id);	    
		    List<Venditore> list =cv.findVenditore(id);
		    List<Posizione> listpos =cp.findPosizione(idpos);
		    assertTrue(list.isEmpty());
		    assertTrue(listpos.isEmpty());
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
	
	@Test
	void testdeleteFormaggioNonOrdinate() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
			ControllerVenditore cv = new ControllerVenditore();
			ControllerProduttore cp = new ControllerProduttore();
			int idven = cv.createVenditore("esselunga", "intensivo");
			int idprod = cp.createProduttore("ciccio", 12345, "intensiva");
			Set<Venditore> ven = new HashSet<Venditore>();
			ven.add(cv.findVenditore(idven).get(0));
			Set<Produttore> prod = new HashSet<Produttore>();
			prod.add(cp.findProduttore(idprod).get(0));
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato", ven, prod);
		    cv.deleteVenditore(idven);
		    cp.deleteProduttore(idprod);
		    cf.delete(id);
		    List<Formaggio> formaggi =cf.findFormaggio(id);
		    List<Venditore> venditori =cv.findVenditore(idven);
		    List<Produttore> posizioni = cp.findProduttore(idprod);
		    assertTrue(formaggi.isEmpty());
		    assertTrue(venditori.isEmpty());
		    assertTrue(posizioni.isEmpty());  
		}
		catch(NomeNullException | PrezzoNullNegativeException e){
			fail();
		}
	}
	
	@Test
	void testdeleteFruttoNonOrdinate() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			ControllerVenditore cv = new ControllerVenditore();
			ControllerProduttore cp = new ControllerProduttore();
			int idven = cv.createVenditore("esselunga", "intensivo");
			int idprod = cp.createProduttore("cicciSSIMO", 22234, "intensiva");
			Set<Venditore> ven = new HashSet<Venditore>();
			ven.add(cv.findVenditore(idven).get(0));
			Set<Produttore> prod = new HashSet<Produttore>();
			prod.add(cp.findProduttore(idprod).get(0));
		    int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", ven, prod);
		    cp.deleteProduttore(idprod);
		    cv.deleteVenditore(idven);
		    cf.delete(id);  
		    List<Frutto> formaggi = cf.findFrutto(id);
		    List<Venditore> venditori =cv.findVenditore(idven);
		    List<Produttore> posizioni = cp.findProduttore(idprod);
		    assertTrue(formaggi.isEmpty());
		    assertTrue(venditori.isEmpty());
		    assertTrue(posizioni.isEmpty()); 
		}
		catch(NomeNullException | PrezzoNullNegativeException e){
			fail();
		}
	}
}
