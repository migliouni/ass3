package Tests;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di testare operazioni di creazione, aggiornamento di entità collegate fra di loro.
 */
class TestCasesRelazioni {
	
	@Test
	void testPosizioneVenditore() {
		try {
			ControllerPosizione cp = new ControllerPosizione();
			int numeroPosizioni = cp.findPosizione().size();
			
			int id  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			int id2 = cp.createPosizione("bornago 27c", "roma", 66666, "Lazio", "RM");
			
			Set<Posizione> posizioni = new HashSet<Posizione>();
			posizioni.add(cp.findPosizione(id).get(0));
			posizioni.add(cp.findPosizione(id2).get(0));
			
			List<Posizione> list =cp.findPosizione();
			assertEquals(list.size(),numeroPosizioni +2);
			
			ControllerVenditore cv = new ControllerVenditore();
			int numeroVenditori = cv.findVenditore().size();
			
			
			int a = cv.createVenditore("esselunga","intensivo", posizioni);
			List<Venditore> listven =cv.findVenditore();
			assertEquals(listven.size(),numeroVenditori +1);
			
			 
			Set<Posizione> posizioniInVenditore = cv.findVenditore(a).get(0).getPosizioni();
			
			List<Integer> codiciPos = new ArrayList<Integer>(posizioniInVenditore.size());
			for(Posizione i : posizioniInVenditore) {
				codiciPos.add(i.getCodicePos());
			}
			
			
			assertEquals(posizioniInVenditore.size(), 2);
			assertTrue(codiciPos.contains(id));
			assertTrue(codiciPos.contains(id2));
			cv.deleteVenditore(a);
		    cp.deletePosizione(id);
		    cp.deletePosizione(id2);
		
		}
		catch(PosizioneNotValidException | NomeNullException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			// TODO Auto-generated catch block
						e.printStackTrace();
			}
		
	}
	
	@Test
	void testVenditoreAddDeleteSocio() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    
			int id = cv.createVenditore("esselunga","intensivo");
		    int id2 = cv.createVenditore("carrefour","intensivo");
		    
		    Venditore socio1 = cv.findVenditore(id).get(0);
		    Venditore socio2 = cv.findVenditore(id2).get(0);
		    
		    Set<Venditore> venditori = new HashSet<Venditore>();
		    venditori.add(socio1);
		    venditori.add(socio2);
		    
	        int b = cv.createVenditore("iper la grande i", "intensivo", venditori);
	        
	        int id3 = cv.createVenditore("Pane e salame","intensivo");
	        
	        Venditore socio3 = cv.findVenditore(id3).get(0);
	        
	        cv.addSocio(b, socio3);
	        assertEquals(cv.findVenditore(b).get(0).getSoci().size(), 3);
	        
	        cv.removeSocio(b, socio2);
	        assertEquals(cv.findVenditore(b).get(0).getSoci().size(), 2);
	        
	        cv.deleteVenditore(b);
	        cv.deleteVenditore(id);
	        cv.deleteVenditore(id2);
	        
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testVenditoreAddDeletePosizione() {
		try {
			
			ControllerPosizione cp = new ControllerPosizione(); 
			
			int id  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			int id2 = cp.createPosizione("bornago 27c", "roma", 66666, "Lazio", "RM");
			
			Set<Posizione> posizioni = new HashSet<Posizione>();
			posizioni.add(cp.findPosizione(id).get(0));
			posizioni.add(cp.findPosizione(id2).get(0));
		    
			ControllerVenditore cv = new ControllerVenditore();
		    int idven = cv.createVenditore("esselunga","intensivo", posizioni);
		    
		    int idPos = cp.createPosizione("mantegna 9", "novara", 28043, "Piemonte", "NO");
		    Posizione pos = cp.findPosizione(idPos).get(0);
		    
		    cv.addPosizione(idven,pos);
		    assertEquals(cv.findVenditore(idven).get(0).getPosizioni().size(), 3);
		    
		    cv.removePosizione(idven, pos);
		    assertEquals(cv.findVenditore(idven).get(0).getPosizioni().size(), 2);
		    
		    cv.deleteVenditore(idven);
		    cp.deletePosizione(id);
		    cp.deletePosizione(id2);
		    cp.deletePosizione(idPos);
		    
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
	
	@Test
	void testProduttoreUpdatePosizione() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
			
			ControllerPosizione cp = new ControllerPosizione();
			int idpos  = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			Posizione pos = cp.findPosizione(idpos).get(0);
			
		    int id = cv.createProduttore("ciccio", 12345, "intensiva",pos);
		    
		    int id2 = cp.createPosizione("bornago 27c", "roma", 66666, "Lazio", "RM");
		    
		    cv.updatePosizione(id, cp.findPosizione(id2).get(0));
		    Posizione posProd = cv.findProduttore(id).get(0).getCodicePosizione();
		    assertEquals(posProd.getCitta(), "roma");
		    
		    cv.deleteProduttore(id);
		    cp.deletePosizione(idpos);
		    cp.deletePosizione(id2);
		    
		}
		catch(NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
	
	@Test
	void testVenditoreProdotto() {
		try {
			ControllerPosizione cp = new ControllerPosizione();
			
			
		    int id = cp.createPosizione("sarca 334","milano", 24245, "Lombardia", "MI");
		    int id2 = cp.createPosizione("dante 13","roma", 66666, "Lazio", "RM");
		    int id3 = cp.createPosizione("petrarca 34","novara", 33333, "Piemonte", "NO");
		    
		    Set<Posizione> posizioni = new HashSet<Posizione>();
		    posizioni.add(cp.findPosizione(id).get(0));
		    posizioni.add(cp.findPosizione(id3).get(0));
		    
		    Set<Posizione> posizioni2 = new HashSet<Posizione>();
		    posizioni2.add(cp.findPosizione(id2).get(0));
		    
		    ControllerVenditore cv = new ControllerVenditore();
		    int numeroVenditori = cv.findVenditore().size();
		    
		    int idven = cv.createVenditore("esselunga","intensivo", posizioni, null);
		    int id2ven = cv.createVenditore("carrefour","intensivo", posizioni2, null);
		    int id3ven = cv.createVenditore("lidl","intensivo", null, null);
		    List<Venditore> list =cv.findVenditore();
		    
			assertEquals(list.size(),numeroVenditori +3);
		    
		    Set<Venditore> venditori = new HashSet<Venditore>();
		    venditori.add(cv.findVenditore(idven).get(0));
		    venditori.add(cv.findVenditore(id2ven).get(0));
		    
		    Set<Venditore> venditori2 = new HashSet<Venditore>();
		    venditori2.add(cv.findVenditore(id3ven).get(0));
		    
		    ControllerFrutto cf = new ControllerFrutto();
		    int numerofrutti = cf.findFrutto().size();
		    
		    ControllerFormaggio cfor = new ControllerFormaggio();
		    int numeroformaggi = cfor.findFormaggio().size();
		    
		    
			int a = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", venditori);
			int b = cfor.createFormaggio(30.0, "parmigiano", 24, "CAPRA", "cagliato", venditori);
			int c = cfor.createFormaggio(30.0, "brie", 25, "CAPRA", "cagliato", venditori2);
			
			List<Frutto> listfrutto =cf.findFrutto();
			assertEquals(listfrutto.size(),numerofrutti +1);
			
			List<Formaggio> listformaggio =cfor.findFormaggio();
			assertEquals(listformaggio.size(), numeroformaggi + 2);
			
			Set<Venditore> venditoriFrutto = new HashSet<Venditore>(cf.findFrutto(a).get(0).getVenditori());
			List<Integer> codiciVenditori = new ArrayList<Integer>(venditoriFrutto.size());
			for(Venditore i : venditoriFrutto) {
				codiciVenditori.add(i.getCodiceVenditore());
			}
			
			
			assertEquals(codiciVenditori.size(), 2);
			assertTrue(codiciVenditori.contains(idven));
			assertTrue(codiciVenditori.contains(id2ven));
			
			cf.delete(a);
		    cfor.delete(b);
		    cfor.delete(c);
			
		    cv.deleteVenditore(idven);
		    cv.deleteVenditore(id2ven);
		    cv.deleteVenditore(id3ven);
			
			cp.deletePosizione(id);
		    cp.deletePosizione(id2);
		    cp.deletePosizione(id3);
		    
		    
		    
		}catch(NomeNullException | PosizioneNotValidException | PrezzoNullNegativeException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
		}
	}
	
	@Test
	void testSocio() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
			int numeroVenditori = cv.findVenditore().size();
		    
			int id = cv.createVenditore("esselunga","intensivo",null, null);
		    int id2 = cv.createVenditore("carrefour","intensivo",null, null);
		    
		    Venditore socio1 = cv.findVenditore(id).get(0);
		    Venditore socio2 = cv.findVenditore(id2).get(0);
		    
		    Set<Venditore> venditori = new HashSet<Venditore>();
		    venditori.add(socio1);
		    venditori.add(socio2);
		    
	        int b = cv.createVenditore("iper la grande i", "intensivo", null, venditori);
	        
	        List<Venditore> list = cv.findVenditore();
			assertEquals(list.size(),numeroVenditori +3);
	        
			Set<Venditore> sociInVenditore = new HashSet<Venditore>(cv.findVenditore(b).get(0).getSoci());
			List<Integer> codiciSoci = new ArrayList<Integer>(sociInVenditore.size());
			for(Venditore i : sociInVenditore) {
				codiciSoci.add(i.getCodiceVenditore());
			}
			
			assertEquals(codiciSoci.size(), 2);
			assertTrue(codiciSoci.contains(id));
			assertTrue(codiciSoci.contains(id2));
			
			cv.deleteVenditore(b);
			cv.deleteVenditore(id);
			cv.deleteVenditore(id2);
			
			
		}
		catch (NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testPosizioneProduttore() {
		try {
			ControllerPosizione cp = new ControllerPosizione();
			int numeroPosizioni = cp.findPosizione().size();
		    
			int id = cp.createPosizione("sarca 334","milano", 24245, "Lombardia", "MI");
		    int id2 = cp.createPosizione("dante 13","roma", 66666, "Lazio", "RM");
		    int id3 = cp.createPosizione("petrarca 34","novara", 33333, "Piemonte", "NO");
		    List<Posizione> listpos = cp.findPosizione();
			assertEquals(listpos.size(),numeroPosizioni +3);
		    
		    Posizione pos1 = cp.findPosizione(id).get(0);
		    Posizione pos2 = cp.findPosizione(id2).get(0);
		    Posizione pos3 = cp.findPosizione(id3).get(0);
		   
		    ControllerProduttore cprod = new ControllerProduttore();
		    int numeroProduttori = cprod.findProduttore().size();
		    
		    int proid =  cprod.createProduttore("ciccio", 12345, "intensiva", pos1);
			int proid1 = cprod.createProduttore("pippo", 23456, "super", pos2);
			int proid2 = cprod.createProduttore("pippo", 13213, "super-iper", pos3);
			
			List<Produttore> listprod =cprod.findProduttore();
			assertEquals(listprod.size(),numeroProduttori +3);
			
			Posizione posprod1 = cprod.findProduttore(proid).get(0).getCodicePosizione();
			Posizione posprod2 = cprod.findProduttore(proid1).get(0).getCodicePosizione();
			Posizione posprod3 = cprod.findProduttore(proid2).get(0).getCodicePosizione();
			
			assertEquals(id, posprod1.getCodicePos());
			assertEquals(id2, posprod2.getCodicePos());
			assertEquals(id3, posprod3.getCodicePos());
			
			cprod.deleteProduttore(proid);
			cprod.deleteProduttore(proid1);
			cprod.deleteProduttore(proid2);
			cp.deletePosizione(id);
			cp.deletePosizione(id2);
			cp.deletePosizione(id3);
			
		}
		catch (NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e) {
			fail();
		}
	}
	
	@Test
	void testProdottoDa() {
		try {
			ControllerPosizione cp = new ControllerPosizione();
		    
			int id = cp.createPosizione("sarca 334","milano", 24245, "Lombardia", "MI");
		    int id2 = cp.createPosizione("dante 13","roma", 66666, "Lazio", "RM");
		    int id3 = cp.createPosizione("petrarca 34","novara", 33333, "Piemonte", "NO");
		    
		    Posizione pos1 = cp.findPosizione(id).get(0);
		    Posizione pos2 = cp.findPosizione(id2).get(0);
		    Posizione pos3 = cp.findPosizione(id3).get(0);
		   
		    ControllerProduttore cprod = new ControllerProduttore();
		    int numeroProduttori = cprod.findProduttore().size();
		    
		    int proid =  cprod.createProduttore("ciccio", 12345, "intensiva", pos1);
			int proid1 = cprod.createProduttore("pippo", 23456, "super", pos2);
			int proid2 = cprod.createProduttore("pippo", 13213, "super-iper", pos3);
			List<Produttore> listprod = cprod.findProduttore();
			assertEquals(listprod.size(),numeroProduttori +3);
		    
			Set<Produttore> produttori = new HashSet<Produttore>();
			produttori.add(cprod.findProduttore(proid).get(0));
			produttori.add(cprod.findProduttore(proid1).get(0));
			
			Set<Produttore> produttori2 = new HashSet<Produttore>();
			produttori2.add(cprod.findProduttore(proid2).get(0));
			
			ControllerFrutto cfrut = new ControllerFrutto();
			int numeroFrutto = cfrut.findFrutto().size();
			int a = cfrut.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", null, produttori);
			List<Frutto> listfrut =cfrut.findFrutto();
			assertEquals(listfrut.size(), numeroFrutto +1);
			
			ControllerFormaggio cfor = new ControllerFormaggio();
			int numeroFormaggio = cfor.findFormaggio().size();
			int b= cfor.createFormaggio(30.0, "parmigiano", 24, "CAPRA", "cagliato", null, produttori2);
			List<Formaggio> listfor =cfor.findFormaggio();
			assertEquals(listfor.size(),numeroFormaggio +1);
			
			
			Set<Produttore> produttoriFrutto = new HashSet<Produttore>(cfrut.findFrutto(a).get(0).getProduttori());
			List<Integer> codiciProduttori = new ArrayList<Integer>(produttoriFrutto.size()); 
			for(Produttore i : produttoriFrutto) {
				codiciProduttori.add(i.getCodiceAzienda());
			}
			
			
			assertEquals(codiciProduttori.size(), 2);
			assertTrue(codiciProduttori.contains(proid));
			assertTrue(codiciProduttori.contains(proid1));
			
			cfrut.delete(a);
		    cfor.delete(b);
			
		    cprod.deleteProduttore(proid);
			cprod.deleteProduttore(proid1);
			cprod.deleteProduttore(proid2);
		    
			cp.deletePosizione(id);
		    cp.deletePosizione(id2);
		    cp.deletePosizione(id3);
			
		    
			
		}
		catch (PrezzoNullNegativeException | NomeNullException | PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e) {
			fail();
		}		
	}
	
	@Test
	void testFruttoUpdateAddDeleteProduttore() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			ControllerProduttore cprod = new ControllerProduttore();
		    
		    int proid =  cprod.createProduttore("ciccio", 12345, "intensiva");
		    
		    Set<Produttore> produttore =  new HashSet<Produttore>(cprod.findProduttore(proid));
			
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita",produttore);
			
			int proid1 = cprod.createProduttore("pippo", 23456, "super");
			Produttore produttoretoAdd = cprod.findProduttore(proid1).get(0);
			
			cf.addProduttore(id, produttoretoAdd);
			
			assertEquals(cf.findFrutto(id).get(0).getProduttori().size(), 2);
			
			cf.removeProduttore(id, produttoretoAdd);
			assertEquals(cf.findFrutto(id).get(0).getProduttori().size(), 1);
			
			cf.delete(id);
			cprod.deleteProduttore(proid);
			cprod.deleteProduttore(proid1);
			
		   
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFruttoUpdateAddDeleteVenditore() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			ControllerVenditore cven = new ControllerVenditore();
		    
		    int proid =  cven.createVenditore("ciccio","intensiva");
		    
		    Set<Venditore> venditore =  new HashSet<Venditore>(cven.findVenditore(proid));
			
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", venditore);
			
			int proid1 = cven.createVenditore("pippo", "super");
			Venditore venditoretoAdd = cven.findVenditore(proid1).get(0);
			
			cf.addVenditore(id, venditoretoAdd);
			
			assertEquals(cf.findFrutto(id).get(0).getVenditori().size(), 2);
			
			cf.removeVenditore(id, venditoretoAdd);
			assertEquals(cf.findFrutto(id).get(0).getVenditori().size(), 1);
			
			cf.delete(id);
			cven.deleteVenditore(proid);
			cven.deleteVenditore(proid1);
			
			
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFormaggioUpdateAddDeleteProduttore() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
			ControllerProduttore cprod = new ControllerProduttore();
		    
		    int proid =  cprod.createProduttore("ciccio", 12345, "intensiva");
		    
		    Set<Produttore> produttore =  new HashSet<Produttore>(cprod.findProduttore(proid));
			
			int id = cf.createFormaggio(30.0, "parmigiano", 24, "CAPRA", "cagliato",produttore);
			
			int proid1 = cprod.createProduttore("pippo", 23456, "super");
			Produttore produttoretoAdd = cprod.findProduttore(proid1).get(0);
			
			cf.addProduttore(id, produttoretoAdd);
			
			assertEquals(cf.findFormaggio(id).get(0).getProduttori().size(), 2);
			
			cf.removeProduttore(id, produttoretoAdd);
			assertEquals(cf.findFormaggio(id).get(0).getProduttori().size(), 1);
			
			cf.delete(id);
			cprod.deleteProduttore(proid);
			cprod.deleteProduttore(proid1);
		   
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFormaggioUpdateAddDeleteVenditore() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
			ControllerVenditore cven = new ControllerVenditore();
		    
		    int proid =  cven.createVenditore("ciccio","intensiva");
		    
		    Set<Venditore> venditore =  new HashSet<Venditore>(cven.findVenditore(proid));
			
			int id = cf.createFormaggio(30.0, "parmigiano", 24, "CAPRA", "cagliato",venditore);
			
			int proid1 = cven.createVenditore("pippo", "super");
			Venditore venditoretoAdd = cven.findVenditore(proid1).get(0);
			
			cf.addVenditore(id, venditoretoAdd);
			
			assertEquals(cf.findFormaggio(id).get(0).getVenditori().size(), 2);
			
			cf.removeVenditore(id, venditoretoAdd);
			assertEquals(cf.findFormaggio(id).get(0).getVenditori().size(), 1);
			
			cf.delete(id);
			cven.deleteVenditore(proid);
			cven.deleteVenditore(proid1);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	

}


