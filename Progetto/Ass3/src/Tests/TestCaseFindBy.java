package Tests;
import static org.junit.jupiter.api.Assertions.*;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di verificare i metodi findBy, ovvero i metodi che fanno query nelle relazioni manytomany per le entità non
 * owner della tabella intermedia generata.
 */
class TestCaseFindBy {	
	@Test
	void testFindByVenditore() {
		try {
		ControllerVenditore cv = new ControllerVenditore();
		   int idven = cv.createVenditore("esselunga","intensivo", null, null);
		    int id2ven = cv.createVenditore("carrefour","intensivo", null, null);
		    int id3ven = cv.createVenditore("lidl","intensivo", null, null);
		    Set<Venditore> venditori = new HashSet<Venditore>();
		    venditori.add(cv.findVenditore(idven).get(0));
		    venditori.add(cv.findVenditore(id2ven).get(0));
		    Set<Venditore> venditori2 = new HashSet<Venditore>();
		    venditori2.add(cv.findVenditore(id3ven).get(0));
		    ControllerFrutto cf = new ControllerFrutto();
		    ControllerFormaggio cfor = new ControllerFormaggio();
			int a = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", venditori);
			int fo = cfor.createFormaggio(30.0, "brie", 25, "CAPRA", "cagliato", venditori2);
			assertEquals(cv.findFruttoByVenditore(idven).get(0).getCodice(), a);
			assertEquals(cv.findFormaggioByVenditore(id3ven).get(0).getCodice(), fo);
			cv.deleteVenditore(idven);
			cv.deleteVenditore(id2ven);
			cv.deleteVenditore(id3ven);
			cf.delete(a);
			cfor.delete(fo);
			
		}catch(PrezzoNullNegativeException | NomeNullException  e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testFindByProduttore() {
		try {
			ControllerProduttore cp = new ControllerProduttore();
		    int idven = cp.createProduttore("ciccio", 12345, "intensiva");
		    int id2ven = cp.createProduttore("ciccio", 12345, "intensiva");
		    int id3ven = cp.createProduttore("ciccio", 12345, "intensiva");
		    Set<Produttore> produttori = new HashSet<Produttore>();
		    produttori.add(cp.findProduttore(idven).get(0));
		    produttori.add(cp.findProduttore(id2ven).get(0));
		    Set<Produttore> produttori2 = new HashSet<Produttore>();
		    produttori2.add(cp.findProduttore(id3ven).get(0));
		    ControllerFrutto cf = new ControllerFrutto();
		    ControllerFormaggio cfor = new ControllerFormaggio();
			int a = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita", produttori);
			int fo = cfor.createFormaggio(30.0, "brie", 25, "CAPRA", "cagliato", produttori2);
			assertEquals(cp.findFruttoByProduttore(idven).get(0).getCodice(), a);
			assertEquals(cp.findFormaggioByProduttore(id3ven).get(0).getCodice(), fo);
			cp.deleteProduttore(idven);
			cp.deleteProduttore(id2ven);
			cp.deleteProduttore(id3ven);
			cf.delete(a);
			cfor.delete(fo);
		
		}catch(PrezzoNullNegativeException | NomeNullException  e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testFindByVenditorePosizione() {
		try {
			
			ControllerPosizione cp = new ControllerPosizione();
			int id = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			int id2 = cp.createPosizione("bornago 27c", "roma", 66666, "Lazio", "RM");
			Set<Posizione> posizioni = new HashSet<Posizione>();
			posizioni.add(cp.findPosizione(id).get(0));
			posizioni.add(cp.findPosizione(id2).get(0));
			ControllerVenditore cv = new ControllerVenditore();
			int a = cv.createVenditore("carrefour","intensivo", posizioni);
			assertEquals(cp.findVenditoriByPosizione(id).get(0).getCodiceVenditore(), a);
			assertEquals(cp.findVenditoriByPosizione(id2).get(0).getCodiceVenditore(), a);
			cp.deletePosizione(id);
			cp.deletePosizione(id2);
			cv.deleteVenditore(a);
		}
		catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException
				| IndirizzoNotValidException | NomeNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}