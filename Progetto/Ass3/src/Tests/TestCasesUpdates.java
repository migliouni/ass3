package Tests;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
/**
 * Classe di test, che permette di testare tutte le operazioni di aggiornamento disponbili su singole entità.
 */
class TestCasesUpdates { 
	
	@Test
	void testPosizioneUpdateindirizzo() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    
		    cp.updateIndirizzo(id, "dante 17");
		    pos = cp.findPosizione(id).get(0);
		    assertEquals(pos.getIndirizzo(), "dante 17");
		    
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testPosizioneUpdateCitta() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    
		    cp.updateCitta(id, "monza");
		    pos = cp.findPosizione(id).get(0);
		    assertEquals(pos.getCitta(), "monza");
		    
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testPosizioneUpdateCap() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    
		    cp.updateCap(id, 22222);
		    pos = cp.findPosizione(id).get(0);
		    assertEquals(pos.getCap(), 22222);
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testPosizioneUpdateRegione() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    
		    cp.updateRegione(id, "Piemonte");
		    pos = cp.findPosizione(id).get(0);
		    assertEquals(pos.getRegione(), "Piemonte");
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testPosizioneUpdateProvincia() {
		try {
			ControllerPosizione cp = new ControllerPosizione(); 
		    int id = cp.createPosizione("libertà 19","milano", 24245, "Lombardia", "MI");
		    Posizione pos= cp.findPosizione(id).get(0);
		    
		    cp.updateProvincia(id, "RO");
		    pos = cp.findPosizione(id).get(0);
		    assertEquals(pos.getProvincia(), "RO");
		    cp.deletePosizione(id);
		}
		catch(PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException | IndirizzoNotValidException e){
			fail();
			}	
	}
	
	@Test
	void testVenditoreUpdateNome() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    int id = cv.createVenditore("esselunga","intensivo");
		    Venditore ven = cv.findVenditore(id).get(0);
		    
		    cv.updateNome(id,"newEsselunga");
		    ven = cv.findVenditore(id).get(0);
		    assertEquals(ven.getNome(), "newEsselunga");
		    cv.deleteVenditore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	
	@Test
	void testVenditoreUpdateTipo() {
		try {
			ControllerVenditore cv = new ControllerVenditore();
		    int id = cv.createVenditore("esselunga","intensivo");
		    Venditore ven = cv.findVenditore(id).get(0);
		    
		    cv.updateTipoVenditore(id,"non intensivo");
		    ven = cv.findVenditore(id).get(0);
		    assertEquals(ven.getTipoVenditore(), "non intensivo");
		    cv.deleteVenditore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	} 
	
	
	@Test
	void testFormaggioUpdatePrezzo() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    cf.updatePrezzo(id, 456.0);
		    f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getPrezzo(), 456.0);
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFormaggioUpdateNome() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    cf.updateNome(id, "brie");
		    f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getNome(), "brie");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFormaggioUpdateStagionatura() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    cf.updateTipoLatte(id, "non vaccino");
		    f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getTipoLatte(), "non vaccino");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	
	@Test
	void testFormaggioUpdateTipoLatte() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    cf.updateTipoLatte(id, "non vaccino");
		    f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getTipoLatte(), "non vaccino");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFormaggioUpdateCaglio() {
		try {
			ControllerFormaggio cf = new ControllerFormaggio();
		    int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
		    Formaggio f = cf.findFormaggio(id).get(0);
		    cf.updateCaglio(id, "non cagliato");
		    f = cf.findFormaggio(id).get(0);
		    assertEquals(f.getCaglio(), "non cagliato");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}	
	}
	
	@Test
	void testFruttoUpdatePrezzo() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findFrutto(id).get(0);
		    
		    cf.updatePrezzo(id,32.0);
		    f = cf.findFrutto(id).get(0);
		    assertEquals(f.getPrezzo(), 32.0);
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}

	@Test
	void testFruttoUpdateNome() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findFrutto(id).get(0);
		    
		    cf.updateNome(id,"mela");
		    f = cf.findFrutto(id).get(0);
		    assertEquals(f.getNome(), "mela");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFruttoUpdateStagione() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findFrutto(id).get(0);
		    
		    cf.updateStagione(id,"estate");
		    f = cf.findFrutto(id).get(0);
		    assertEquals(f.getStagione(), "estate");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFruttoUpdateColore() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findFrutto(id).get(0);
		    
		    cf.updateColore(id,"verde");
		    f = cf.findFrutto(id).get(0);
		    assertEquals(f.getColore(), "verde");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testFruttoUpdate() {
		try {
			ControllerFrutto cf = new ControllerFrutto();
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			Frutto f = cf.findFrutto(id).get(0);
		    
		    cf.updateVarieta(id, "melinda");
		    f = cf.findFrutto(id).get(0);
		    assertEquals(f.getVarieta(), "melinda");
		    cf.delete(id);
		}
		catch (PrezzoNullNegativeException | NomeNullException e) {
			fail();
		}		
	}
	
	@Test
	void testProduttoreUpdateNome() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    Produttore pr = cv.findProduttore(id).get(0);
		    cv.updateNome(id, "pluto");
		    pr = cv.findProduttore(id).get(0);
		    assertEquals(pr.getNome(), "pluto");
		    cv.deleteProduttore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testProduttoreUpdatePiva() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    Produttore pr = cv.findProduttore(id).get(0);
		    cv.updatePIva(id, 13124);
		    pr = cv.findProduttore(id).get(0);
		    assertEquals(pr.getPartitaIva(), 13124);
		    cv.deleteProduttore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	@Test
	void testProduttoreUpdateTipo() {
		try {
			ControllerProduttore cv = new ControllerProduttore();
		    int id = cv.createProduttore("ciccio", 12345, "intensiva");
		    Produttore pr = cv.findProduttore(id).get(0);
		    cv.updateTipoProduzione(id, "non intensiva");
		    pr = cv.findProduttore(id).get(0);
		    assertEquals(pr.getTipoProduzione(), "non intensiva");
		    cv.deleteProduttore(id);
		}
		catch(NomeNullException e){
			fail();
		}
	}
	
	
	
}
