package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import Controllers.ControllerFrutto;
import Controllers.ControllerPosizione;
import Controllers.ControllerProduttore;
import Controllers.ControllerVenditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
import Views.ViewFormaggio;
import Views.ViewFrutto;
import Views.ViewPosizione;
import Views.ViewProduttore;
import Views.ViewVenditore;
/**
 * Classe di test, che permette di testare tutte le operazioni di stampa.
 * Anche se non è vero test, ci permette di verificare le operazioni di formattazione della stampa
 */
class TestViews {
	
	@Test
	void testViewFormaggio() {
		ControllerFormaggio cf = new ControllerFormaggio();
		try {
			int id = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato");
			ViewFormaggio vw = new ViewFormaggio();
			vw.print(cf.findFormaggio(id));
		   
		    ControllerVenditore cv = new ControllerVenditore();
		    
		    int idven = cv.createVenditore("esselunga","intensivo");
		    int id2ven = cv.createVenditore("carrefour","intensivo");
		    
		    Set<Venditore> venditori = new HashSet<Venditore>();
		    venditori.add(cv.findVenditore(idven).get(0));
		    venditori.add(cv.findVenditore(id2ven).get(0));
			
		    int id2 = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato",venditori );
		    vw.printJoinVen(cf.findFormaggio(id2));
			
			ControllerProduttore cprod = new ControllerProduttore();
			int proid =  cprod.createProduttore("ciccio", 12345, "intensiva");
			
			Set<Produttore> produttori = new HashSet<Produttore>();
		    produttori.add(cprod.findProduttore(proid).get(0));
			
			int id3 = cf.createFormaggio(30.0, "parmigiano", 12, "vaccino", "cagliato",produttori );
			vw.printJoinProd(cf.findFormaggio(id3));
			
			cf.delete(id);
			cf.delete(id2);
			cf.delete(id3);
			cv.deleteVenditore(idven);
			cv.deleteVenditore(id2ven);
			cprod.deleteProduttore(proid);
		} catch (PrezzoNullNegativeException e) {
			// TODO Auto-generated catch block
		} catch (NomeNullException e) {
			// TODO Auto-generated catch block
		}
	}
	
	@Test
	void testViewFrutto() {
		ControllerFrutto cf = new ControllerFrutto();
		try {
			int id = cf.createFrutto(30.0, "banana", "inverno", "giallo", "cichita");
			ViewFrutto vw = new ViewFrutto();
			vw.print(cf.findFrutto(id));
		   
		    ControllerVenditore cv = new ControllerVenditore();
		    
		    int idven = cv.createVenditore("esselunga","intensivo");
		    int id2ven = cv.createVenditore("carrefour","intensivo");
		    
		    Set<Venditore> venditori = new HashSet<Venditore>();
		    venditori.add(cv.findVenditore(idven).get(0));
		    venditori.add(cv.findVenditore(id2ven).get(0));
			
		    int id2 = cf.createFrutto(34.0, "mela", "inverno", "verde", "melinda",venditori );
		    vw.printJoinVen(cf.findFrutto(id2));
			
			ControllerProduttore cprod = new ControllerProduttore();
			int proid =  cprod.createProduttore("ciccio", 12345, "intensiva");
			
			Set<Produttore> produttori = new HashSet<Produttore>();
		    produttori.add(cprod.findProduttore(proid).get(0));
			
			int id3 = cf.createFrutto(34.0, "arancia", "inverno", "arancione", "Siciliana", produttori );
			vw.printJoinProd(cf.findFrutto(id3));
			
			cf.delete(id);
			cf.delete(id2);
			cf.delete(id3);
			cv.deleteVenditore(idven);
			cv.deleteVenditore(id2ven);
			cprod.deleteProduttore(proid);
		} catch (PrezzoNullNegativeException e) {
			// TODO Auto-generated catch block
		} catch (NomeNullException e) {
			// TODO Auto-generated catch block
		}
	}
	
	@Test
	void testViewPosizione() {
		ControllerPosizione cp = new ControllerPosizione();
		int id;
		try {
			ViewPosizione vw = new ViewPosizione();
			
			id = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
			vw.print(cp.findPosizione(id));
			int id2 = cp.createPosizione("bornago 27c", "roma", 66666, "Lazio", "RM");
			vw.print(cp.findPosizione(id2));
			
			cp.deletePosizione(id);
			cp.deletePosizione(id2);
			
		} catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException
				| IndirizzoNotValidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	void testViewVenditore() {
			ControllerPosizione cp = new ControllerPosizione();
			int id;
			try {
				id = cp.createPosizione("libertà 199","milano", 24245, "Lombardia", "MI");
				
				int id2= cp.createPosizione("bornago 27c", "roma", 66666, "Lazio", "RM");
				Set<Posizione> pos = new HashSet<Posizione>();
				pos.add(cp.findPosizione(id).get(0));
				
				 ViewVenditore vw = new ViewVenditore();
				 ControllerVenditore cv = new ControllerVenditore();
				 int idven = cv.createVenditore("esselunga","intensivo");
				 int id2ven = cv.createVenditore("carrefour","intensivo",pos);
				 
				 vw.print(cv.findVenditore(idven));
				 vw.printJoinPos(cv.findVenditore(id2ven));
				 vw.print(cv.findVenditore());
				 
				 cp.deletePosizione(id);
				 cp.deletePosizione(id2);
				 cv.deleteVenditore(idven);
				 cv.deleteVenditore(id2ven);
				
			} catch (PosizioneNotValidException | CapNotValidException | ProvinciaNotValidException
					| IndirizzoNotValidException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NomeNullException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	@Test
	void testViewProduttore() {
		ControllerProduttore cprod = new ControllerProduttore();
		ViewProduttore vw = new ViewProduttore();
		try {
			int proid =  cprod.createProduttore("ciccio", 12345, "intensiva");
			int proi2 =  cprod.createProduttore("pluto", 12345, "intensiva");
			vw.print(cprod.findProduttore(proid));
			vw.print(cprod.findProduttore());
			
			cprod.deleteProduttore(proid);
			cprod.deleteProduttore(proi2);
			
		} catch (NomeNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
