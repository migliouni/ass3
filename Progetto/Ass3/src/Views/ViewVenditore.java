package Views;
import Beans.Posizione;
import Beans.Prodotto;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerVenditore;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
* Classe che permette di visualizzare in forma tabellare il contenuto di un venditore.
**/
public class ViewVenditore {
	/**
	 * Visualizza una lista di oggetti di classe Venditore con una visualizzazione a tabella
	 * @param lista di venditori da visualizzare
	 */
	public void print(List<Venditore> list) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "TIPO_VEND");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
	    
		for(Venditore venditore:list) {
			System.out.format("%10s %10s %20s", 
				 	venditore.getCodiceVenditore(), 
		    		venditore.getNome(),
		    		venditore.getTipoVenditore());
				System.out.println();
		}
	}
	/**
	 * Visualizza una lista di oggetti di classe Venditore e la reliativa posizione con una visualizzazione a tabella
	 * @param lista di venditori da visualizzare
	 */
	public void printJoinPos(List<Venditore> list) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %20s %20s %15s %15s", "CODICE", "NOME", "TIPO_VEND", "CITTA'", "PROVINCIA");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
	    
	    for(Venditore venditore: list) {
	    	Set<Posizione> posizioni = venditore.getPosizioni();
	    	if(posizioni.size() == 0) {
	    		System.out.format("%10s %10s %20s %15s %15s", 
					 	venditore.getCodiceVenditore(), 
			    		venditore.getNome(),
			    		venditore.getTipoVenditore(),
			    		"NULL",
			    		"NULL");
					System.out.println();
	    	}else {
	    		for(Posizione posizione: posizioni) {
	    			System.out.format("%10s %20s %20s %10s %10s", 
						 	venditore.getCodiceVenditore(), 
				    		venditore.getNome(),
				    		venditore.getTipoVenditore(),
				    		posizione.getCitta(),
				    		posizione.getProvincia());
						System.out.println();
	    		}
	    	}
	    }
	}
	/**
	 * Restituisce la lista di prodotti associata al venditore che ha il codice passato come parametro
	 * @param codice di un venditore
	 * @return lista di prodotti
	 */
	public List<Prodotto> getProdotti(int codiceVenditore){
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Ass3" );
		   EntityManager entitymanager = emfactory.createEntityManager();

		   //Scalar function
		   Query query = entitymanager.createQuery("Select f from Prodotto f join f.venditori v where v.codiceVenditore = " + codiceVenditore);
		   List<Prodotto> list = query.getResultList();

		   return list;
	}
	/**
	 * Visualizza una lista di oggetti di classe Venditore e i suoi relativi prodotti con una visualizzazione a tabella
	 * @param lista di venditori da visualizzare
	 */
	public void printJoinProdotto(List<Venditore> venditori) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "PRODOTTO");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
		    
		for(Venditore venditore: venditori) {
			for(Prodotto prodotto: getProdotti(venditore.getCodiceVenditore())) {
				System.out.format("%10s %10s %20s",
						venditore.getCodiceVenditore(),
						venditore.getNome(),
						prodotto.getNome());
				System.out.println();
			}
		}
	}
	/**
	 * Visualizza una lista di oggetti di classe Venditore e i relativi soci con una visualizzazione a tabell
	 * @param lista di venditori da visualizzare 
	 */
	public void printJoinSocio(List<Venditore> list) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s %10s %10s", "CODICE", "NOME", "TIPO_VEND", "SOCIO", "TIPO_VEN_SOCIO");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
	    
		for(Venditore venditore:list) {
			Set<Venditore> soci = venditore.getSoci();
			if(soci.size() == 0) {
				System.out.format("%10s %10s %20s %10s %10s", 
					 	venditore.getCodiceVenditore(), 
			    		venditore.getNome(),
			    		venditore.getTipoVenditore(),
			    		"NULL",
			    		"NULL");
					System.out.println();
			}else {
				for(Venditore v: soci) {
					System.out.format("%10s %10s %20s %10s %10s", 
						 	venditore.getCodiceVenditore(), 
				    		venditore.getNome(),
				    		venditore.getTipoVenditore(),
				    		v.getNome(),
				    		v.getTipoVenditore());
						System.out.println();
				}
			}
		}
	}
}