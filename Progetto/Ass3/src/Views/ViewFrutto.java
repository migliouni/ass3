package Views;
import Beans.Frutto;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFrutto;
import java.util.List;
import java.util.Set;

/**
* Classe che permette di visualizzare in forma tabellare il contenuto di un prodotto di tipo Frutto.
**/
public class ViewFrutto {
	/**
	 * Visualizza una lista di oggetti di classe Frutto 
	 * @param lista di frutti da visualizzare
	 */
	public void print(List<Frutto> list) {
		System.out.println("---------------------------------------------------------------------------------------");
		System.out.printf("%10s %10s %20s %15s %10s %10s", "CODICE", "PREZZO", "NOME", "STAGIONE", "COLORE", "VARIETA'");
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------");
		for(Frutto frutto:list) {
			System.out.format("%10s %10g %20s %15s %10s %10s", 
					  frutto.getCodice(), 
					  frutto.getPrezzo(), 
					  frutto.getNome(), 
					  frutto.getStagione(),
					  frutto.getColore(),
					  frutto.getVarieta());
				System.out.println();
		}
	}
	/**
	 * Visualizza una lista di oggetti di classe Frutto con i relativi produttori
	 * @param lista di frutti da visualizzare
	 */
	public void printJoinProd(List<Frutto> frutti) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "PRODUTTORE");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
		
		for(Frutto frutto:frutti) {
			Set<Produttore> p = frutto.getProduttori();
			if(p.size() == 0) {
				System.out.format("%10s %10s %20s", 
					 	frutto.getCodice(),
					 	frutto.getNome(),
					 	"NULL");
					System.out.println();
			}else {
				for(Produttore v: p) {
					System.out.format("%10s %10s %20s", 
							frutto.getCodice(),
						 	frutto.getNome(),
						 	v.getNome());
						System.out.println();
				}
			}
		}
	}
	/**
	 * Visualizza una lista di oggetti di classe Frutto con i relativi venditori
	 * @param lista di frutti da visualizzare
	 */
	public void printJoinVen(List<Frutto> frutti) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "VENDITORE");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
		
		for(Frutto frutto:frutti) {
			Set<Venditore> p = frutto.getVenditori();
			if(p.size() == 0) {
				System.out.format("%10s %10s %20s", 
					 	frutto.getCodice(),
					 	frutto.getNome(),
					 	"NULL");
					System.out.println();
			}else {
				for(Venditore v: p) {
					System.out.format("%10s %10s %20s", 
							frutto.getCodice(),
						 	frutto.getNome(),
						 	v.getNome());
						System.out.println();
				}
			}
		}
	}
}
