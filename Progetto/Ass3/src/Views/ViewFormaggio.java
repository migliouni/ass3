package Views;
import Beans.Formaggio;
import Beans.Produttore;
import Beans.Venditore;
import Controllers.ControllerFormaggio;
import java.util.List;
import java.util.Set;

/**
 * Classe che permette di visualizzare in forma tabellare il contenuto di un prodotto di tipo Formaggio.
 **/
public class ViewFormaggio {
	/**
	 * Visualizza una lista di entità di classe Formaggio con la visualizzazione a tabella
	 * @param lista di formaggio da visualizzare
	 */
	public void print(List<Formaggio> list) {
        System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s %15s %10s %10s", "CODICE", "PREZZO", "NOME", "STAGIONATURA", "TIPOLATTE", "CAGLIO");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
		for(Formaggio formaggio:list) {
		    System.out.format("%10s %10g %20s %15d %10s %10s", 
		    		formaggio.getCodice(), 
		    		formaggio.getPrezzo(), 
		    		formaggio.getNome(), 
		    		formaggio.getStagionatura(),
		    		formaggio.getTipoLatte(),
		    		formaggio.getCaglio());
		    System.out.println();
		}
	}
	
	/**
	 * Visualizza una lista di oggetti di classe Formaggio con i relativi produttori
	 * @param lista di formaggio da visualizzare
	 */
	public void printJoinProd(List<Formaggio> formaggi) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "PRODUTTORE");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
	    
		for(Formaggio formaggio:formaggi) {
			Set<Produttore> p = formaggio.getProduttori();
			if(p.size() == 0) {
				System.out.format("%10s %10s %20s", 
					 	formaggio.getCodice(),
					 	formaggio.getNome(),
					 	"NULL");
					System.out.println();
			}else {
				for(Produttore v: p) {
					System.out.format("%10s %10s %20s", 
							formaggio.getCodice(),
						 	formaggio.getNome(),
						 	v.getNome());
						System.out.println();
				}
			}
		}
	}
	
	/**
	 * Visualizza una lista di oggetti di classe Formaggio con i relativi venditori
	 * @param lista di formaggio da visualizzare
	 */
	public void printJoinVen(List<Formaggio> formaggi) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "VENDITORE");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
	    
		for(Formaggio formaggio:formaggi) {
			Set<Venditore> p = formaggio.getVenditori();
			if(p.size() == 0) {
				System.out.format("%10s %10s %20s", 
					 	formaggio.getCodice(),
					 	formaggio.getNome(),
					 	"NULL");
					System.out.println();
			}else {
				for(Venditore v: p) {
					System.out.format("%10s %10s %20s", 
							formaggio.getCodice(),
						 	formaggio.getNome(),
						 	v.getNome());
						System.out.println();
				}
			}
		}
		
	}
}
