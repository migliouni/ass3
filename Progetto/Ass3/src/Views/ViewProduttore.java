package Views;
import Beans.Produttore;
import Beans.Prodotto;
import Controllers.ControllerProduttore;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
* Classe che permette di visualizzare in forma tabellare il contenuto di un produttore.
**/
public class ViewProduttore {
	/**
	 * Visualizza una lista di oggetti di classe Produttore con una visualizzazione a tabell
	 * @param lista di produttori da visualizzare 
	 */
	public void print(List<Produttore> list) {
		String pos;
		System.out.println("---------------------------------------------------------------------------------------");
		   System.out.printf("%10s %10s %20s %15s %10s %10s %10s %10s", "CODICE", "NOME", "PIVA", "TIPO_PROD", "CAP", "CITTAì", "PROVINCIA", "REGIONE");
		   System.out.println();
		   System.out.println("---------------------------------------------------------------------------------------");
		for(Produttore produttore:list) {
			if(produttore.getCodicePosizione() == null) {
				System.out.format("%10s %10s %20d %15s %10s %10s %10s %10s", 
						  produttore.getCodiceAzienda(), 
						  produttore.getNome(),
						  produttore.getPartitaIva(),
						  produttore.getTipoProduzione(),
						  "NULL",
						  "NULL",
						  "NULL",
						  "NULL");	
					System.out.println();
			}else {
				System.out.format("%10s %10s %20d %15s %10s %10s %10s %10s", 
					  produttore.getCodiceAzienda(), 
					  produttore.getNome(),
					  produttore.getPartitaIva(),
					  produttore.getTipoProduzione(),
					  produttore.getCodicePosizione().getCap(),
					  produttore.getCodicePosizione().getCitta(),
					  produttore.getCodicePosizione().getProvincia(),
					  produttore.getCodicePosizione().getRegione());	
				System.out.println();
			}
		}
	}
	/**
	 * Restituisce una lista di prodotti associata al produttore che ha il codice passato come parametro
	 * @param codice di un produttore
	 * @return lista di prodotti
	 */
	public List<Prodotto> getProdotti(int codiceAzienda){
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Ass3" );
		   EntityManager entitymanager = emfactory.createEntityManager();

		   //Scalar function
		   Query query = entitymanager.createQuery("Select f from Prodotto f join f.produttori p where p.codiceAzienda = " + codiceAzienda);
		   List<Prodotto> list = query.getResultList();

		   return list;
	}
	/**
	 * Visualizza una lista di oggetti di classe Produttore e i relativi prodotti con una visualizzazione a tabell
	 * @param lista di produttori da visualizzare 
	 */
	public void printJoinProdotto(List<Produttore> produttori) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s", "CODICE", "NOME", "PRODOTTO");
	    System.out.println();
	    System.out.println("---------------------------------------------------------------------------------------");
		    
		for(Produttore produttore:produttori) {
			for(Prodotto prodotto: getProdotti(produttore.getCodiceAzienda())) {
				System.out.format("%10s %10s %20s",
						produttore.getCodiceAzienda(),
						produttore.getNome(),
						prodotto.getNome());
				System.out.println();
			}
		}
	}
}
