package Views;
import Beans.Posizione;
import Controllers.ControllerPosizione;
import java.util.List;

/**
* Classe che permette di visualizzare in forma tabellare il contenuto di una posizione.
**/
public class ViewPosizione {
	/**
	 * Visualizza una lista di oggetti di classe Posizione con la visualizzazione a tabella
	 * @param lista di posizioni
	 */
	public void print(List<Posizione> list) {
		System.out.println("---------------------------------------------------------------------------------------");
	    System.out.printf("%10s %10s %20s %15s %10s", "CODICE", "CITTA", "CAP", "REGIONE", "PROVINCIA");
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------");
		 
		for(Posizione posizione:list) {
			System.out.format("%10s %10s %20d %15s %10s", 
					posizione.getCodicePos(), 
					posizione.getCitta(),
					posizione.getCap(),
					posizione.getRegione(),
					posizione.getProvincia());
			System.out.println();
		}
	}
}
