package Controllers;
import Beans.Frutto;
import Beans.Venditore;
import Exceptions.NomeNullException;
import Exceptions.PrezzoNullNegativeException;
import Beans.Produttore;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Query; 
/**
* Classe che permette di gestire(creare,modificare, leggere, trovare) un prodotto di tipo Frutto in tutti i suoi attributi, nel database.
* Permette di persistere sul database gli oggetti di tipo Frutto. Estende la classe ControllerProdotto
**/
public class ControllerFrutto extends ControllerProdotto {
	/** 
	  * Metodo che permette di creare e persistire un frutto, partendo dagli attributi passati.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param stringa stagione in cui cresce il frutto.
	  * @param Stringa colore del frutto.
	  * @param Stringa varietà del frutto, nome specifico.
	  * @param Insieme di venditori associati al prodotto.
	  * @param Insieme di produttori associati al prodotto.
	  * @return intero identificativo del prodotto.
	**/
	public int createFrutto(
			double prezzo, 
			String nome, 
			String stagione, 
			String colore, 
			String varieta, 
			Set<Venditore> venditori, 
			Set<Produttore> produttori) throws PrezzoNullNegativeException, NomeNullException {
		EntityManager entitymanager = inizializePersistance();
	    Frutto frutto = new Frutto( ); 
	    if(prezzo > 0)
	    	frutto.setPrezzo(prezzo);
	    else
	    	throw new PrezzoNullNegativeException();
	    if(nome!= null)
	    	frutto.setNome(nome);
	    else
	    	throw new NomeNullException();
	    frutto.setStagione(stagione);
	    frutto.setColore(colore);
	    frutto.setVarieta(varieta);
	    frutto.setVenditori(venditori);
	    frutto.setProduttori(produttori);
	    entitymanager.persist(frutto);
	    entitymanager.getTransaction( ).commit( );
	    closePersistance(entitymanager);
	    return frutto.getCodice();
	}
	/** 
	  * Metodo che permette di creare e persistire un frutto, senza dover specificare i produttori e i venditori.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param stringa stagione in cui cresce il frutto.
	  * @param Stringa colore del frutto.
	  * @param Stringa varietà del frutto, nome specifico.
	  * @param Insieme di venditori o produttori.
	  * @return intero identificativo del prodotto.
	**/
	public int createFrutto(double prezzo, String nome, String stagione, String colore, String varieta) throws PrezzoNullNegativeException, NomeNullException {
		return createFrutto(prezzo, nome, stagione, colore, varieta, null, null);
	}
	/** 
	  * Metodo templato che permette di creare e persistire un frutto, senza dover specificare i produttori o i venditori.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param stringa stagione in cui cresce il frutto.
	  * @param Stringa colore del frutto.
	  * @param Stringa varietà del frutto, nome specifico.
	  * @param Insieme di venditori o produttori.
	  * @return intero identificativo del prodotto.
	**/
	public <T> int createFrutto(double prezzo, String nome, String stagione, String colore, String varieta, Set<T> classe) throws PrezzoNullNegativeException, NomeNullException{
		  Iterator<T> i = classe.iterator();
		  if(i.next().getClass().toString().equals("class Beans.Venditore"))
			  return createFrutto(prezzo, nome, stagione, colore, varieta, (Set<Venditore>) classe, null);
		  else
			  return createFrutto(prezzo, nome, stagione, colore, varieta, null, (Set<Produttore>) classe);
	} 
	
	/** 
	  * Metodo che permette di aggiornare un frutto, con gli attributi passati.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param identificativo del frutto da aggiornare.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param stringa stagione in cui cresce il frutto.
	  * @param Stringa colore del frutto.
	  * @param Stringa varietà del frutto, nome specifico.
	  * @param Insieme di venditori associati al prodotto.
	  * @param Insieme di produttori associati al prodotto.
	**/
   public void updateFrutto(int idFrutto, double prezzo, String nome, String stagione, String colore, String varieta) throws PrezzoNullNegativeException, NomeNullException {
	   EntityManager entitymanager = inizializePersistance();
	   Frutto frutto = entitymanager.find( Frutto.class, idFrutto);
	   if(frutto != null) {
		   if(prezzo > 0)
		    	frutto.setPrezzo(prezzo);
		    else
		    	throw new PrezzoNullNegativeException();
		    if(nome!= null)
		    	frutto.setNome(nome);
		    else
		    	throw new NomeNullException();
		   frutto.setStagione(stagione);
		   frutto.setColore(colore);
		   frutto.setVarieta(varieta);
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di aggiornare un frutto, con gli attributi passati.
	  * @param identificativo del frutto da aggiornare.
	  * @param stringa stagione in cui cresce il frutto.
	**/
   public void updateStagione(int idFrutto, String stagione) {
	   EntityManager entitymanager = inizializePersistance();
	   Frutto frutto = entitymanager.find( Frutto.class, idFrutto);
	   if(frutto != null) {
		   frutto.setStagione(stagione);
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   /** 
	  * Metodo che permette di aggiornare un frutto, con gli attributi passati.
	  * @param identificativo del frutto da aggiornare.
	  * @param Stringa colore del frutto.
	**/
   public void updateColore(int idFrutto, String colore) {
	   EntityManager entitymanager = inizializePersistance();
	   Frutto frutto = entitymanager.find( Frutto.class, idFrutto);
	   if(frutto != null) {
		   frutto.setColore(colore);
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   /** 
	  * Metodo che permette di aggiornare un frutto, con gli attributi passati.
	  * @param identificativo del frutto da aggiornare.
	  * @param Stringa varietà del frutto, nome specifico.
	**/
   public void updateVarieta(int idFrutto, String varieta) {
	   EntityManager entitymanager = inizializePersistance();
	   Frutto frutto = entitymanager.find( Frutto.class, idFrutto);
	   if(frutto != null) {
		   frutto.setVarieta(varieta);
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   /** 
	  * Metodo che permette di recuperare la lista formata dal frutto con l'id passato.
	  * @param identificativo del frutto da ricercare.
	  * @return lista formata dal frutto con id passato se presente.
	**/
   public List<Frutto> findFrutto(int id) {
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Frutto f where f.codice = " + id);
	   List<Frutto> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata da tutti i frutti.
	  * @return la lista formata da tutti i frutti.
	**/
   public List<Frutto> findFrutto(){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Frutto f");
	   List<Frutto> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata dai frutti con un determinato nome.
	  * @param nome di frutto da ricercare.
	  * @return lista formata dal frutti con il nome passato.
	**/
   public List<Frutto> findByName(String nome){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Frutto f where f.nome LIKE '" + nome + "'");
	   List<Frutto> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata dai frutti con prezzo maggiore di quello passato.
	  * @param prezzo da confrontare.
	  * @return lista formata dai frutti con il prezzo maggiore di quello passato.
	**/
   public List<Frutto> findByUpperPrice(int prezzo){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Frutto f where f.prezzo <= '" + prezzo + "'");
	   List<Frutto> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
}
