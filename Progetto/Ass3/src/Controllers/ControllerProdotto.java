package Controllers;
import javax.persistence.EntityManager;
import Beans.Prodotto;
import Beans.Produttore;
import Beans.Venditore;
import Exceptions.NomeNullException;
import Exceptions.PrezzoNullNegativeException;
 
/**
* Classe che comprende tutti i metodi applicabili a Oggetti di tipo Prodotto. I controller dei prodotti specifici(Formaggio,Frutto ecc)
* estederanno questa classe ed aggiungeranno metodi specifici per la determinata classe. Previene la duplicazione del codice in ControllerFrutto 
* e ControllerFormaggio e nei possibili futuri controllers.
**/
public abstract class ControllerProdotto extends ControllerBase {

	 /** 
	  	* Metodo che permette di aggiornare il prezzo di un prodotto.
	  	* @throws PrezzoNullNegativeException.
	  	* @param identificativo del prodotto da aggiornare.
	  	* @param nuovo prezzo da associare al prodotto.
	  **/
	  public void updatePrezzo(int idprodotto, double prezzo) throws PrezzoNullNegativeException {
		   EntityManager entitymanager = inizializePersistance();
		   Prodotto prodotto = entitymanager.find(Prodotto.class, idprodotto);
		   if(prodotto != null) {
			   if(prezzo > 0)
			    	prodotto.setPrezzo(prezzo);
			    else
			    	throw new PrezzoNullNegativeException();
			   entitymanager.getTransaction( ).commit( );
		   }
		   closePersistance(entitymanager);
	   }
	  
	   /** 
		  * Metodo che permette di aggiornare il nome di un prodotto.
		  * @throws  NomeNullException.
		  * @param identificativo del prodotto da aggiornare.
		  * @param Stringa del nuovo nome del prodotto.
		**/
	   public void updateNome(int idProdotto, String nome) throws NomeNullException {
		   EntityManager entitymanager = inizializePersistance();
		   Prodotto prodotto = entitymanager.find(Prodotto.class, idProdotto);
		   if(prodotto != null) {
			    if(nome!= null) {
			    	prodotto.setNome(nome);
			    }
			    else
			    	throw new NomeNullException();
			   entitymanager.getTransaction( ).commit( );
		   }
		   closePersistance(entitymanager);
	   }

	   /** 
		  * Metodo che permette di eliminare un prodotto.
		  * @param identificativo del prodotto da eliminare.
		**/
	   public void delete(int idProdotto) {
		    EntityManager entitymanager = inizializePersistance();
		    Prodotto prodottoToDelete = entitymanager.find(Prodotto.class, idProdotto);
		    if(prodottoToDelete != null) {
		    	entitymanager.remove(prodottoToDelete);
		    	entitymanager.getTransaction().commit();
		    }
		    closePersistance(entitymanager);
	   }
	   
	   /** 
		  * Metodo che permette di aggiungere un venditore.
		  * @param identificativo del prodotto da aggiornare.
		  * @param Oggetto di tipo venditore da aggiungere all'insieme dei venditori.
		**/
	   public void addVenditore(int idProdotto, Venditore venditore) {
		   EntityManager entitymanager = inizializePersistance();
		   Prodotto prodotto = entitymanager.find(Prodotto.class, idProdotto);
		   if(prodotto != null) {
			   prodotto.addVenditore(venditore);
			   entitymanager.getTransaction( ).commit( );
		   }
		   closePersistance(entitymanager);
	   }
	   /** 
		  * Metodo che permette di eliminare un venditore.
		  * @param identificativo del prodotto da aggiornare.
		  * @param Oggetto di tipo venditore da eliminare dall'insieme dei venditori.
		**/
	   public void removeVenditore(int idProdotto, Venditore venditore) {
		   EntityManager entitymanager = inizializePersistance();
		   Prodotto prodotto = entitymanager.find(Prodotto.class, idProdotto);
		   if(prodotto != null) {
			   for(Venditore v: prodotto.getVenditori()){
				   if(v.getCodiceVenditore() == venditore.getCodiceVenditore()) {
					   prodotto.removeVenditore(v);
					   break;
				   }
			   }
			   entitymanager.getTransaction( ).commit( );
		   }
		   closePersistance(entitymanager);
	   }
	   
	   /** 
		  * Metodo che permette di aggiungere un produttore.
		  * @param identificativo del prodotto da aggiornare.
		  * @param Oggetto di tipo produttore da aggiungere all'insieme dei produttori.
		**/
	   public void addProduttore(int idProdotto, Produttore produttore) {
		   EntityManager entitymanager = inizializePersistance();
		   Prodotto prodotto = entitymanager.find(Prodotto.class, idProdotto);
		   if(prodotto != null) {
			   prodotto.addProduttore(produttore);
			   entitymanager.getTransaction( ).commit( );
		   }
		   closePersistance(entitymanager);
	   }
	   
	   /** 
		  * Metodo che permette di eliminare un produttore.
		  * @param identificativo del prodotto da aggiornare.
		  * @param Oggetto di tipo produttore da eliminare dall'insieme dei produttori.
		**/
	   public void removeProduttore(int idProdotto, Produttore produttore) {
		   EntityManager entitymanager = inizializePersistance();
		   Prodotto prodotto = entitymanager.find(Prodotto.class, idProdotto);
		   if(prodotto != null) {
			   for(Produttore p: prodotto.getProduttori()){
				   if(p.getCodiceAzienda() == produttore.getCodiceAzienda()) {
					   prodotto.removeProduttore(p);
					   break;
				   }
			   }
			   entitymanager.getTransaction( ).commit( );
		   }
		   closePersistance(entitymanager);
	   }
}

