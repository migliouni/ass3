package Controllers;
import Beans.Posizione;
import Beans.Produttore;
import Beans.Venditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.ProvinciaNotValidException;
import java.beans.Beans;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
/**
* Classe che permette di gestire(creare,modificare, leggere, trovare) una posizione in tutti i suoi attributi, nel database.
* Permette di persistere sul database gli oggetti di tipo Posizione.
**/
public class ControllerPosizione extends ControllerBase {
	
	/** 
	  * Metodo che permette di creare e persistire una posizione, partendo dagli attributi passati.
	  * @throws PosizioneNotValidException.
	  * @throws CapNotValidException.
	  * @throws ProvinciaNotValidException.
	  * @throws IndirizzoNotValidException.
	  * @param String indirizzio della posizione, via e numero.
	  * @param String nome della città.
	  * @param intero di 5 cifre che rappresenta il CAP.
	  * @param String con il nome della regione.
	  * @param String della provicncia di due caratteri.
	  * @return intero identificativo della posizione.
	**/
	public int createPosizione(
			String indirizzo, 
			String citta, 
			int cap, 
			String regione, 
			String provincia) throws 
	PosizioneNotValidException, 
	CapNotValidException, 
	ProvinciaNotValidException, 
	IndirizzoNotValidException {
		EntityManager entitymanager = inizializePersistance();
	    Posizione Posizione = new Posizione();
	    if(indirizzo != null)
	    	Posizione.setIndirizzo(indirizzo);
	    else 
	    	throw new IndirizzoNotValidException();
	    if(citta != null)
	    	Posizione.setCitta(citta);
	    else
	    	throw new PosizioneNotValidException();
	    if(Integer.toString(cap).length() == 5)
	    	Posizione.setCap(cap);
	    else
	    	throw new CapNotValidException();
	    Posizione.setRegione(regione);
	    if(provincia.length() == 2)
	    	Posizione.setProvincia(provincia);
	    else
	    	throw new ProvinciaNotValidException();
		entitymanager.persist(Posizione);
    	entitymanager.getTransaction( ).commit( );
	    closePersistance(entitymanager);	
	    return Posizione.getCodicePos();
	}
	
	/** 
	  * Metodo che permette di eliminare una posizione passata tramite identificativo.
	  * @param intero identificativo della posizione.
	**/
	public void deletePosizione(int idPosizione) {
    	Posizione PosizioneToDelete = findPosizione(idPosizione).get(0);
	    if(PosizioneToDelete != null) {
	    	pre_remove(idPosizione);
	    	EntityManager entitymanager = inizializePersistance();
	    	Posizione delete = entitymanager.find(Posizione.class, idPosizione);
	    	entitymanager.remove(delete);
	    	entitymanager.getTransaction().commit();
	    	closePersistance(entitymanager);
	    }
	    
    }
	/** 
	  * Metodo privato che assicura di eliminare la posizione con id specificato nelle relazioni in cui
	  * è coinvolta prima di procedere alla sua eliminazione.
	  * @param intero per identificare la posizione da eliminare.
	**/ 
	private void pre_remove(int idpos) {
		//verificare in produttori
		ControllerProduttore cp = new ControllerProduttore();
		List<Produttore> produttori = cp.findProduttore();
		if (produttori != null) {
			for (Produttore p : produttori) {
				if(p.getCodicePosizione() != null && p.getCodicePosizione().getCodicePos() == idpos) {
					cp.updatePosizione(p.getCodiceAzienda(), null);
				}
			}
		}
		
		Posizione posizione = this.findPosizione(idpos).get(0);
		//verificare in venditori
		ControllerVenditore cv = new ControllerVenditore();
		List<Venditore> venditori = cv.findVenditore();
		if (venditori != null) {
			for (Venditore v : venditori) {
				for (Posizione p: v.getPosizioni())
					if (p.getCodicePos() == idpos) {
						cv.removePosizione(v.getCodiceVenditore(), posizione);
					}
				}
		}
	}
	
	/** 
	  * Metodo che permette di aggiornare una posizione presente nel db, partendo dagli attributi passati.
	  * @throws PosizioneNotValidException.
	  * @throws CapNotValidException.
	  * @throws ProvinciaNotValidException.
	  * @throws IndirizzoNotValidException.
	  * @param Intero identificativo della Posizione
	  * @param String indirizzio della posizione, via e numero.
	  * @param String nome della città.
	  * @param intero di 5 cifre che rappresenta il CAP.
	  * @param String con il nome della regione.
	  * @param String della provicncia di due caratteri.
	**/
	public void updatePosizione(int idPosizione, String indirizzo, String citta, int cap, String regione, String provincia) 
			throws ProvinciaNotValidException, CapNotValidException, PosizioneNotValidException, IndirizzoNotValidException {
	   EntityManager entitymanager = inizializePersistance();
	   Posizione posizione = entitymanager.find( Posizione.class, idPosizione);
	   if(posizione != null) {
		   if(indirizzo != null)
		    	posizione.setIndirizzo(indirizzo);
		    else 
		    	throw new IndirizzoNotValidException();
		    if(citta != null)
		    	posizione.setCitta(citta);
		    else
		    	throw new PosizioneNotValidException();
		    if(Integer.toString(cap).length() == 5)
		    	posizione.setCap(cap);
		    else
		    	throw new CapNotValidException();
		    posizione.setRegione(regione);
		    if(provincia.length() == 2)
		    	posizione.setProvincia(provincia);
		    else
		    	throw new ProvinciaNotValidException();
		   entitymanager.getTransaction( ).commit( );
		   closePersistance(entitymanager);
	   }	   
    }
	
	/** 
	  * Metodo che permette di aggiornare l'indirizzo di una posizione presente nel db.
	  * @throws IndirizzoNotValidException.
	  * @param Intero identificativo della Posizione
	  * @param String indirizzio della posizione, via e numero.
	**/
	public void updateIndirizzo(int idPosizione, String indirizzo) throws IndirizzoNotValidException {
		EntityManager entitymanager = inizializePersistance();
		Posizione posizione = entitymanager.find( Posizione.class, idPosizione);
		if(posizione != null) {
			if(indirizzo != null)
				posizione.setIndirizzo(indirizzo);
			else 
				throw new IndirizzoNotValidException();
			entitymanager.getTransaction( ).commit( );
		}   
		closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di aggiornare la città di una posizione presente nel db.
	  * @throws PosizioneNotValidException.
	  * @param Intero identificativo della Posizione
	  * @param String nome della città.
	**/
	public void updateCitta(int idPosizione, String citta) throws PosizioneNotValidException {
		EntityManager entitymanager = inizializePersistance();
		Posizione posizione = entitymanager.find( Posizione.class, idPosizione);
		if(posizione != null) {
			if(citta != null)
				posizione.setCitta(citta);
		    else
		    	throw new PosizioneNotValidException();
		   entitymanager.getTransaction( ).commit( );
	   }	   
	   closePersistance(entitymanager);
    }
	
	/** 
	  * Metodo che permette di aggiornare il CAP di una posizione presente nel db.
	  * @throws CapNotValidException.
	  * @param Intero identificativo della Posizione
	  * @param intero di 5 cifre che rappresenta il CAP.
	**/
	public void updateCap(int idPosizione, int cap) throws CapNotValidException {
		EntityManager entitymanager = inizializePersistance();
		Posizione posizione = entitymanager.find( Posizione.class, idPosizione);
		if(posizione != null) {
			if(Integer.toString(cap).length() == 5)
				posizione.setCap(cap);
			else
				throw new CapNotValidException();
			entitymanager.getTransaction( ).commit( );
		}  
		closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di aggiornare la regione di una posizione presente nel db.
	  * @param Intero identificativo della Posizione
	  * @param String con il nome della regione.
	**/
	public void updateRegione(int idPosizione, String regione) {
		EntityManager entitymanager = inizializePersistance();
		Posizione posizione = entitymanager.find( Posizione.class, idPosizione);
		if(posizione != null) {
			posizione.setRegione(regione);
			entitymanager.getTransaction( ).commit( );
		}
		closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di aggiornare una posizione presente nel db, partendo dagli attributi passati.
	  * @throws ProvinciaNotValidException.
	  * @param Intero identificativo della Posizione
	  * @param String della provicncia di due caratteri.
	**/
	public void updateProvincia(int idPosizione, String provincia) throws ProvinciaNotValidException {
		EntityManager entitymanager = inizializePersistance();
		Posizione posizione = entitymanager.find( Posizione.class, idPosizione);
		if(posizione != null) {
			if(provincia.length() == 2)
				posizione.setProvincia(provincia);
			else
				throw new ProvinciaNotValidException();
			entitymanager.getTransaction( ).commit( );
		}
		closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata dalla posizione con l'id passato.
	  * @param identificativo della posizione da ricercare.
	  * @return lista formata dalla posizione con id passato se presente.
	**/
	public List<Posizione> findPosizione(int id) {
		EntityManager entitymanager = inizializePersistance();
		Query query = entitymanager.createQuery("Select f from Posizione f where f.codicePosizione = " + id);
	    List<Posizione> list = query.getResultList();
	    closePersistance(entitymanager);
    	return list;
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata da tutte le posizioni presenti nel db.
	  * @return lista formata da tutte le posizioni.
	**/
	public List<Posizione> findPosizione(){
		EntityManager entitymanager = inizializePersistance();
	    Query query = entitymanager.createQuery("Select f from Posizione f");
	    List<Posizione> list = query.getResultList();
  	    closePersistance(entitymanager);
	    return list;
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata dalle posizioni con il nome passato come parametro.
	  * @param Stringa col nome della città da ricercare.
	  * @return lista formata dalle posizioni con il nome di città passato.
	**/
	public List<Posizione> findByName(String citta){
		EntityManager entitymanager = inizializePersistance();
		Query query = entitymanager.createQuery("Select f from Posizione f where f.citta LIKE '" + citta + "'");
		List<Posizione> list = query.getResultList();
		closePersistance(entitymanager);
		return list;
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata dai venditori che sono localizzati nella posizione con id passata.
	  * @param identificativo della posizione da ricercare.
	  * @return lista formata da tutti i venditori nella posizione.
	**/
	public List<Venditore> findVenditoriByPosizione(int idpos){
		EntityManager entitymanager = inizializePersistance();
		Query query = entitymanager.createQuery("Select v from Venditore v join v.posizioni p where p.codicePosizione = " + idpos);
		List<Venditore> get = query.getResultList();
		closePersistance(entitymanager);
	    return get;
	}
	
	
	
}
