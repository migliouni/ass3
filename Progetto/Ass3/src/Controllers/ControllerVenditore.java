package Controllers;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Venditore;
import Exceptions.NomeNullException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Query;
 
/**
* Classe che permette di gestire(creare,modificare, leggere, trovare) un venditore in tutti i suoi attributi, nel database.
* Permette di persistere sul database gli oggetti di tipo Venditore, gestisce le relazioni con le posizioni e con i soci.
**/
public class ControllerVenditore extends ControllerBase {
	
	/** 
	  * Metodo che permette di creare un venditore, partendo dagli attributi passati.
	  * @throws NomeNullException
	  * @param String nome del venditore.
	  * @param String tipo di venditore.
	  * @param Insieme di posizioni del venditore.
	  * @param Insieme di venditori soci del venditore.
	  * @return intero identificativo del venditore.
	**/
	public int createVenditore(String nome, String tipoVenditore, Set<Posizione> posizioni, Set<Venditore> soci) throws NomeNullException {
		EntityManager entitymanager = inizializePersistance();
	    Venditore Venditore = new Venditore( ); 
	    if (nome!= null)
	    	Venditore.setNome(nome);
	    else
	    	throw new NomeNullException();
	    Venditore.setTipoVenditore(tipoVenditore);
	    Venditore.setPosizioni(posizioni);
	    Venditore.setSoci(soci);
	    entitymanager.persist(Venditore);	
	    entitymanager.getTransaction( ).commit( );
	    closePersistance(entitymanager);   
	    return Venditore.getCodiceVenditore();
	}
	
	/** 
	   * Overloading del metodo createVenditore permette di creare un venditore, passando nome e tipo venditore.
	   * @throws NomeNullException
	   * @param String nome del venditore.
	   * @param String tipo di venditore.
	   * @return intero identificativo del venditore.
	**/
	public int createVenditore(String nome, String tipoVenditore) throws NomeNullException {
		 
		return createVenditore(nome, tipoVenditore, null, null);
	}
	
	/** 
	  * Overloading del metodo che permette di creare un venditore, partendo dagli attributi passati. Metodo templato per poter
	  * inferire a run-time la classe(Posizione, Venditore) per poter creare un venditore coi suoi soci, o le sue posizioni.
	  * @throws NomeNullException
	  * @param String nome del venditore.
	  * @param String tipo di venditore.
	  * @param Insieme di oggetti templati T che a run-time viene classificato.
	  * @return intero identificativo del venditore.
	 **/
	public <T> int createVenditore(String nome, String tipoVenditore, Set<T> classe) throws NomeNullException{
		  Iterator<T> i = classe.iterator();
		  if(i.next().getClass().toString().equals("class Beans.Venditore"))
			  return createVenditore(nome, tipoVenditore, null, (Set<Venditore>) classe);
		  else
			  return createVenditore(nome, tipoVenditore, (Set<Posizione>) classe, null);
	} 
	
	/** 
	  * Metodo che permette di eliminare il venditore specificato tramite l'identificativo.
	  * @param identificativo del venditore da eliminare.
	 **/
	public void deleteVenditore(int idVenditore) {
		Venditore VenditoreToDelete = findVenditore(idVenditore).get(0);
	    if(VenditoreToDelete != null) {
	    	pre_remove(idVenditore);
	    	EntityManager entitymanager = inizializePersistance();
		    Venditore VenToDelete = entitymanager.find(Venditore.class, idVenditore);
	    	entitymanager.remove(VenToDelete);
	    	entitymanager.getTransaction().commit();
	    	closePersistance(entitymanager);   
	    }
	      
   }
	
	/** 
	  * Metodo privato che assicura di eliminare il venditore con id specificato nelle relazioni in cui
	  * è coinvolto prima di procedere alla sua eliminazione.
	  * @param intero per identificare il venditore da eliminare.
	**/ 
	private void pre_remove(int idven) {
		Venditore ven = this.findVenditore(idven).get(0);
		//verificare in venditori per soci
		ControllerVenditore cv = new ControllerVenditore();
		List<Venditore> venditori = cv.findVenditore();
		if (venditori != null) {
			for (Venditore v : venditori) {
				for (Venditore p: v.getSoci())
					if (p.getCodiceVenditore() == idven) {
						cv.removeSocio(v.getCodiceVenditore(), ven);
						break;
					}
				}
		}
		//verificare in formaggi
		ControllerFormaggio cfor = new ControllerFormaggio();
		List<Formaggio> formaggi = cfor.findFormaggio();
		if (formaggi != null) {
			for (Formaggio v : formaggi) {
				for (Venditore p: v.getVenditori())
					if (p.getCodiceVenditore() == idven) {
						cfor.removeVenditore(v.getCodice(), ven);
						break;
					}
				}
		}
		//verificare in frutto
		ControllerFrutto cf = new ControllerFrutto();
		List<Frutto> frutti = cf.findFrutto();
		if (frutti != null) {
			for (Frutto v : frutti) {
				for (Venditore p: v.getVenditori())
					if (p.getCodiceVenditore() == idven) {
						cf.removeVenditore(v.getCodice(), ven);
						break;
					}
				}
		}
	}
	/** 
	  * Metodo che permette di aggiornare un venditore con gli attributi passati. 
	  * @throws NomeNullException
	  * @param identificativo del venditore da aggiornare.
	  * @param String nome del venditore.
	  * @param String tipo di venditore.
	**/
	public void updateVenditore(int idVenditore, String nome, String tipoVenditore) throws NomeNullException {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find( Venditore.class, idVenditore);
	   if(venditore != null) {
		   if (nome!= null)
		    	venditore.setNome(nome);
		    else
		    	throw new NomeNullException();
		   venditore.setTipoVenditore(tipoVenditore);
		   entitymanager.getTransaction( ).commit( );
		   closePersistance(entitymanager);
	   }
	}
	
	/** 
	  * Metodo che permette di aggiornare il nome di un venditore con l'attributi passato. 
	  * @throws NomeNullException
	  * @param identificativo del venditore da aggiornare.
	  * @param String nome del venditore.
	**/
   public void updateNome(int idVenditore, String nome) throws NomeNullException {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find( Venditore.class, idVenditore);
	   if(venditore != null) {
		   if (nome!= null)
			   venditore.setNome(nome);
		    else
		    	throw new NomeNullException();
		   entitymanager.getTransaction( ).commit( );
		   closePersistance(entitymanager);
	   }
   }
   
   /** 
	  * Metodo che permette di aggiornare il tipo venditore con l'attributo passato. 
	  * @param identificativo del venditore da aggiornare.
	  * @param String tipo di venditore
	**/
   public void updateTipoVenditore(int idVenditore, String tipoVenditore) {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find( Venditore.class, idVenditore);
	   if(venditore != null) {
		   venditore.setTipoVenditore(tipoVenditore);
		   entitymanager.getTransaction( ).commit( );
		   closePersistance(entitymanager);
	   }
   }
   
    /** 
	  * Metodo che permette di aggiungere un socio di minoranza. 
	  * @param identificativo del venditore da aggiornare.
	  * @param Venditore, socio da aggiungere
	**/
   public void addSocio(int idVenditore, Venditore socio) {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find(Venditore.class, idVenditore);
	   if(venditore != null) {
		   venditore.addSocio(socio);
	   }
	   entitymanager.getTransaction( ).commit( );
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di rimuovere un socio di minoranza. 
	  * @param identificativo del venditore da aggiornare.
	  * @param Venditore, socio da rimuovere
	**/
   public void removeSocio(int idVenditore, Venditore socio) {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find( Venditore.class, idVenditore);
	   if(venditore != null) {
		   for(Venditore s: venditore.getSoci()){
			   if(s.getCodiceVenditore() == socio.getCodiceVenditore()) {
				   venditore.removeSocio(s);
				   break;
			   }
		   }
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di aggiungere una posizione ad un venditore. 
	  * @param identificativo del venditore da aggiornare.
	  * @param Posizione, posizione da aggiungere
	**/
   public void addPosizione(int idVenditore, Posizione posizione) {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find( Venditore.class, idVenditore);
	   if(venditore != null) {
		   venditore.addPosizione(posizione);
		   entitymanager.getTransaction( ).commit( );
	
	   }
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di rimuovere una posizione ad un venditore. 
	  * @param identificativo del venditore da aggiornare.
	  * @param Posizione, posizione da rimuovere
	**/
   public void removePosizione(int idVenditore, Posizione posizione) {
	   EntityManager entitymanager = inizializePersistance();
	   Venditore venditore = entitymanager.find( Venditore.class, idVenditore);
	   if(venditore != null) {
		   for(Posizione p: venditore.getPosizioni()){
			   if(p.getCodicePos() == posizione.getCodicePos()) {
				   venditore.removePosizione(p);
				   break;
			   }
		   }
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata dal venditore con l'id passato.
	  * @param identificativo del venditore da ricercare.
	  * @return lista formata dal venditore con id passato se presente.
	**/
   public List<Venditore> findVenditore(int id) {
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Venditore f where f.codiceVenditore = " + id);
	   List<Venditore> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata da tutti i venditori memorizzati.
	  * @return lista di venditori formata da tutti i venditori
	**/
   public List<Venditore> findVenditore(){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Venditore f");
	   List<Venditore> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista dei venditori con un determinato nome.
	  * @return lista di venditori formata da tutti i venditori con il nome passato.
	**/
   public List<Venditore> findByName(String nome){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Venditore f where f.nome LIKE '" + nome + "'");
	   List<Venditore> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata da tutti i frutti che il venditore con
	  * id passato vende.
	  * @param identificativo del venditore da ricercare.
	  * @return lista formata da tutti i frutti che il venditore vende.
	**/
   public List<Frutto> findFruttoByVenditore(int idven){
	   EntityManager entitymanager = inizializePersistance();
		Query query = entitymanager.createQuery("Select f from Frutto f join f.venditori v where v.codiceVenditore = " + idven);
		List<Frutto> get = query.getResultList();
		closePersistance(entitymanager);
		return get;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata da tutti i formaggi che il venditore con
	  * id passato vende.
	  * @param identificativo del venditore da ricercare.
	  * @return lista formata da tutti i formaggi che il venditore vende.
	**/
   public List<Formaggio> findFormaggioByVenditore(int idven){
	   EntityManager entitymanager = inizializePersistance();
		Query query = entitymanager.createQuery("Select f from Formaggio f join f.venditori v where v.codiceVenditore = " + idven);
		List<Formaggio> get = query.getResultList();
		closePersistance(entitymanager);
		return get;
   }
   
}
