package Controllers;
import Beans.Produttore;
import Beans.Venditore;
import Exceptions.CapNotValidException;
import Exceptions.IndirizzoNotValidException;
import Exceptions.NomeNullException;
import Exceptions.PosizioneNotValidException;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.ProvinciaNotValidException;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Posizione;
import Beans.Prodotto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
/**
* Classe che permette di gestire(creare,modificare, leggere, trovare) una posizione in tutti i suoi attributi, nel database.
* Permette di persistere sul database gli oggetti di tipo Posizione.
**/
public class ControllerProduttore extends ControllerBase {
	
	/** 
	  * Metodo che permette di creare e persistire un produttore, partendo dagli attributi passati.
	  * @throws NomeNullException.
	  * @param String nome del produttore.
	  * @param intero per rappresentare la partita IVA del produttore.
	  * @param String tipo di produzionde del produttore.
	  * @param Oggetto di tipo Posizione, che rappresenta la località del produttore.
	  * @return intero identificativo della produttore.
	**/
	public int createProduttore(String nome, int partitaIva, String tipoProduzione, Posizione pos) throws NomeNullException {
		EntityManager entitymanager = inizializePersistance();
	    Produttore Produttore = new Produttore();
	    if(nome != null)
	    	Produttore.setNome(nome);
	    else
	    	throw new NomeNullException();
	    Produttore.setPartitaIva(partitaIva);
	   	Produttore.setTipoProduzione(tipoProduzione);
	   	Produttore.setCodicePosizione(pos); 
	    entitymanager.persist(Produttore);
	    entitymanager.getTransaction( ).commit( );
	    closePersistance(entitymanager);
	    return Produttore.getCodiceAzienda();
	}
	
	/** 
	  * Overloading del metodo che permette di creare e persistire un produttore, partendo dagli attributi passati, metodo che non prevede la posizione.
	  * @throws NomeNullException.
	  * @param String nome del produttore.
	  * @param intero per rappresentare la partita IVA del produttore.
	  * @param String tipo di produzionde del produttore.
	  * @return intero identificativo della produttore.
	**/
	public int createProduttore(String nome, int partitaIva, String tipoProduzione) throws NomeNullException {
		return createProduttore(nome, partitaIva,tipoProduzione, null);
	}
	
	/** 
	  * Metodo che permette di eliminare il produttore con id specificato.
	  * @param intero per identificare il produttore da eliminare.
	**/
	public void deleteProduttore(int codiceProduttore) {
		Produttore ProduttoreToDelete = findProduttore(codiceProduttore).get(0);
		if(ProduttoreToDelete != null) {
			pre_remove(codiceProduttore);
			EntityManager entitymanager = inizializePersistance();
			Produttore PrToDelete = entitymanager.find(Produttore.class, codiceProduttore);
			entitymanager.remove(PrToDelete);
			entitymanager.getTransaction().commit();
			closePersistance(entitymanager);
		}
		
	}
	
	/** 
	  * Metodo privato che assicura di eliminare il produttore con id specificato nelle relazioni in cui
	  * è coinvolto prima di procedere alla sua eliminazione.
	  * @param intero per identificare il produttore da eliminare.
	**/ 
	private void pre_remove(int idpro) {
		Produttore pro = this.findProduttore(idpro).get(0);
		//verificare in formaggi
		ControllerFormaggio cfor = new ControllerFormaggio();
		List<Formaggio> formaggi = cfor.findFormaggio();
		if (formaggi != null) {
			for (Formaggio v : formaggi) {
				for (Produttore p: v.getProduttori())
					if (p.getCodiceAzienda() == idpro) {
						cfor.removeProduttore(v.getCodice(), pro);
						break;
					}
				}
		}
		//verificare in frutto
		ControllerFrutto cf = new ControllerFrutto();
		List<Frutto> frutti = cf.findFrutto();
		if (frutti != null) {
			for (Frutto v : frutti) {
				for (Produttore p: v.getProduttori())
					if (p.getCodiceAzienda() == idpro) {
						cf.removeProduttore(v.getCodice(), pro);
						break;
					}
				}
		}
	}
	
	/** 
	  * Metodo che permette di aggiornare il produttore con id specificato, con i nuovi attributi passati.
	  * @throws NomeNullException.
	  * @param intero per identificare il produttore da aggiornare.
	  * @param String nome del produttore.
	  * @param intero per rappresentare la partita IVA del produttore.
	  * @param String tipo di produzionde del produttore.
	**/
	public void updateProduttore(int idProd, String nome, int partitaIva, String tipoProduzione) throws NomeNullException {
		EntityManager entitymanager = inizializePersistance();
	    Produttore produttore = entitymanager.find(Produttore.class, idProd);
	    if(produttore != null) {
	    	if(nome != null)
		    	produttore.setNome(nome);
		    else
		    	throw new NomeNullException();
	        produttore.setPartitaIva(partitaIva);
	        produttore.setTipoProduzione(tipoProduzione);
		    entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di aggiornare la posizione del produttore specificato tramite identificativo.
	  * @param intero per identificare il produttore da aggiornare..
	  * @param Oggetto di tipo Posizione.
	**/
	public void updatePosizione(int idProd, Posizione pos) {
		EntityManager entitymanager = inizializePersistance();
		Produttore produttore = entitymanager.find(Produttore.class, idProd);
		if(produttore != null) {
			produttore.setCodicePosizione(pos);
			entitymanager.getTransaction( ).commit( );
		}
		closePersistance(entitymanager);
	}

	/** 
	  * Metodo che permette di aggiornare il nome del produttore con id specificato.
	  * @throws NomeNullException.
	  * @param intero per identificare il produttore da aggiornare.
	  * @param String nome del produttore.
	**/
	public void updateNome(int idProd, String nome) throws NomeNullException {
	   EntityManager entitymanager = inizializePersistance();
	   Produttore produttore = entitymanager.find(Produttore.class, idProd);
	   if(produttore != null) {
		   if(nome != null)
		    	produttore.setNome(nome);
		    else
		    	throw new NomeNullException();
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di aggiornare la partita IVA del produttore con id specificato
	  * @param intero per identificare il produttore da aggiornare
	  * @param intero per rappresentare la partita IVA del produttore.
	**/
	public void updatePIva(int idProd, int partitaIva) {
	   EntityManager entitymanager = inizializePersistance();
	   Produttore produttore = entitymanager.find(Produttore.class, idProd);
	   if(produttore != null) {
	       produttore.setPartitaIva(partitaIva);
		   entitymanager.getTransaction( ).commit( );
	   }   
	   closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di aggiornare il tipo di produzione del produttore specificato tramite id.
	  * @param intero per identificare il produttore da aggiornare.
	  * @param String tipo di produzionde del produttore.
	**/
	public void updateTipoProduzione(int idProd, String tipoProduzione) {
	   EntityManager entitymanager = inizializePersistance();
	   Produttore produttore = entitymanager.find(Produttore.class, idProd);
	   if(produttore != null) {
	       produttore.setTipoProduzione(tipoProduzione);
		   entitymanager.getTransaction( ).commit( );
	   }	   
	   closePersistance(entitymanager);
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata dal produttore con l'id passato.
	  * @param identificativo del produttore da ricercare.
	  * @return lista formata dal produttore con id passato se presente.
	**/
	public List<Produttore> findProduttore(int id) {
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Produttore f where f.codiceAzienda = " + id);
	   List<Produttore> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata da tutti i produttori.
	  * @return lista formata da tutti i produttori presenti.
	**/
	public List<Produttore> findProduttore(){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Produttore f");
	   List<Produttore> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata da tutti i produttore con il nome passato.
	  * @param stringa con il nome del produttore da ricercare.
	  * @return la lista formata da tutti i produttore con il nome passato.
	**/
	public List<Produttore> findByName(String nome){
		EntityManager entitymanager = inizializePersistance();
		Query query = entitymanager.createQuery("Select f from Produttore f where f.nome LIKE '" + nome + "'");
		List<Produttore> list = query.getResultList();
		closePersistance(entitymanager);
		return list;
	}
	
	/** 
	  * Metodo che permette di recuperare la lista formata da tutti i frutti che il produttore con
	  * id passato produce.
	  * @param identificativo del produttore da ricercare.
	  * @return lista formata da tutti i frutti che il produttore produce.
	**/
	public List<Frutto> findFruttoByProduttore(int idpro){
		   EntityManager entitymanager = inizializePersistance();
			
			Query query = entitymanager.createQuery("Select f from Frutto f join f.produttori v where v.codiceAzienda = " + idpro);
			List<Frutto> get = query.getResultList();
			closePersistance(entitymanager);
			return get;
	   }
	
	/** 
	  * Metodo che permette di recuperare la lista formata da tutti i formaggi che il produttore con
	  * id passato produce.
	  * @param identificativo del produttore da ricercare.
	  * @return lista formata da tutti i formaggi che il produttore produce.
	**/
	public List<Formaggio> findFormaggioByProduttore(int idpro){
		EntityManager entitymanager = inizializePersistance();
			
		Query query = entitymanager.createQuery("Select f from Formaggio f join f.produttori v where v.codiceAzienda = " + idpro);
		List<Formaggio> get = query.getResultList();
		closePersistance(entitymanager);
		return get;
	}
}
