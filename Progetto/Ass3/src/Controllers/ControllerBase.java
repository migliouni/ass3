package Controllers; 
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
* Classe che permette di gestire l'inizializzazione e la chiusura dell' EntityManager, i controller delle entità
* estenderanno questa classe, per evitare codice replicato.
**/
public abstract class ControllerBase {
	/**
	* Attributo privato della classe, che permette di gestire l'interazione con il database.
	**/
	protected EntityManagerFactory emfactory;
	
	/** 
	  * Metodo che permette di inizializzare il manager delle entità, metodo privato d'uso interno alla classe.
	  * @return restituisce il gestore delle entità inizializzato.
	 **/
	protected EntityManager inizializePersistance(){
		emfactory = Persistence.createEntityManagerFactory("Ass3");
		EntityManager entitymanager = emfactory.createEntityManager( );
	    entitymanager.getTransaction( ).begin( );
	    return entitymanager;
	}
	
	/** 
	  * Metodo che permette di chiudere correttamente il manager delle entità, metodo privato d'uso interno alla classe.
	  * @param manager delle entità da chiudere.
	 **/
	protected void closePersistance(EntityManager entitymanager) {
	    entitymanager.close( );
	    emfactory.close( );
	}
}
