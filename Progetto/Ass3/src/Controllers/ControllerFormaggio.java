package Controllers;
import Beans.Formaggio;
import Beans.Frutto;
import Beans.Produttore;
import Beans.Venditore;
import Exceptions.PrezzoNullNegativeException;
import Exceptions.NomeNullException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
/**
* Classe che permette di gestire(creare,modificare, leggere, trovare) un prodotto di tipo Formaggio in tutti i suoi attributi, nel database.
* Permette di persistere sul database gli oggetti di tipo Formaggio. Estende la classe ControllerProdotto
**/
public class ControllerFormaggio extends ControllerProdotto {
	/** 
	  * Metodo che permette di creare e persistire un formaggio, partendo dagli attributi passati.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param intero, mesi di stagionatura del formaggio.
	  * @param String tipo di latte usato per fare il formaggio.
	  * @param String caglio, usato per il formaggio.
	  * @param Insieme di venditori associati al prodotto.
	  * @param Insieme di produttori associati al prodotto.
	  * @return intero identificativo del prodotto.
	**/
	public int createFormaggio(
			double prezzo,
			String nome, 
			int stagionatura, 
			String tipoLatte, 
			String caglio, 
			Set<Venditore> venditori, 
			Set<Produttore> produttori) throws PrezzoNullNegativeException, NomeNullException {
		
		EntityManager entitymanager = inizializePersistance();
	    Formaggio Formaggio = new Formaggio( ); 
	    if(prezzo > 0)
	    	Formaggio.setPrezzo(prezzo);
	    else
	    	throw new PrezzoNullNegativeException();
	    if(nome!= null)
	    	Formaggio.setNome(nome);
	    else
	    	throw new NomeNullException();
	    Formaggio.setStagionatura(stagionatura);
	    Formaggio.setTipoLatte(tipoLatte);
	    Formaggio.setCaglio(caglio);
	    Formaggio.setVenditori(venditori);
	    Formaggio.setProduttori(produttori);
	    entitymanager.persist(Formaggio);
	    entitymanager.getTransaction( ).commit( );
	    closePersistance(entitymanager);
	    return Formaggio.getCodice();
	}
	/** 
	  * Metodo tempato che permette di creare e persistire un formaggio specificando solo i venditori o produttori.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param intero, mesi di stagionatura del formaggio.
	  * @param String tipo di latte usato per fare il formaggio.
	  * @param String caglio, usato per il formaggio.
	  * @param Insieme di venditori o produttori associati al prodotto.
	  * @return intero identificativo del prodotto.
	**/
	public <T> int createFormaggio(double prezzo, String nome, int stagionatura, String tipoLatte, String caglio, Set<T> classe) throws PrezzoNullNegativeException, NomeNullException{
		  Iterator<T> i = classe.iterator();
		  if(i.next().getClass().toString().equals("class Beans.Venditore"))
			  return createFormaggio(prezzo, nome, stagionatura, tipoLatte, caglio, (Set<Venditore>) classe, null);
		  else
			  return createFormaggio(prezzo, nome, stagionatura, tipoLatte, caglio, null, (Set<Produttore>) classe);
		} 
	
	/** 
	  * Metodo che permette di creare e persistire un formaggio senza venditori e produttori.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param intero, mesi di stagionatura del formaggio.
	  * @param String tipo di latte usato per fare il formaggio.
	  * @param String caglio, usato per il formaggio.
	  * @return intero identificativo della prodotto.
	**/
	public int createFormaggio(double prezzo, String nome, int stagionatura, String tipoLatte, String caglio) throws PrezzoNullNegativeException, NomeNullException{
		return createFormaggio(prezzo, nome, stagionatura, tipoLatte, caglio, null, null);
	}
	
	/** 
	  * Metodo che permette di aggiornare un formaggio con i nuovi attributi.
	  * @throws PrezzoNullNegativeException.
	  * @throws NomeNullException.
	  * @param identificativo del prodotto da aggiornare.
	  * @param prezzo da associare al prodotto.
	  * @param String nome da associare al prodotto.
	  * @param intero, mesi di stagionatura del formaggio.
	  * @param String tipo di latte usato per fare il formaggio.
	  * @param String caglio, usato per il formaggio.
	**/
	public void updateFormaggio(int idFormaggio, double prezzo, String nome, int stagionatura, String tipoLatte, String caglio) throws PrezzoNullNegativeException, NomeNullException {
		EntityManager entitymanager = inizializePersistance();
		Formaggio formaggio = entitymanager.find( Formaggio.class, idFormaggio);
		if(formaggio != null) {
			if(prezzo > 0)
				formaggio.setPrezzo(prezzo);
		    else
		    	throw new PrezzoNullNegativeException();
		    if(nome!= null)
		    	formaggio.setNome(nome);
		    else
		    	throw new NomeNullException();
		    formaggio.setStagionatura(stagionatura);
		    formaggio.setTipoLatte(tipoLatte);
		    formaggio.setCaglio(caglio);
		    entitymanager.getTransaction( ).commit( );		   
	   } 
	   closePersistance(entitymanager);
   }
   
	/** 
	  * Metodo che permette di aggiornare la stagionatura di un formaggio.
	  * @param identificativo del prodotto.
	  * @param intero, mesi di stagionatura del formaggio.
	**/
   public void updateStagionatura(int idFormaggio, int stagionatura) {
	   EntityManager entitymanager = inizializePersistance();
	   Formaggio Formaggio = entitymanager.find( Formaggio.class, idFormaggio);
	   if(Formaggio != null) {
		   Formaggio.setStagionatura(stagionatura);
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di aggiornare il tipo di latte utilizzato per il formaggio.
	  * @param identificativo del prodotto.
	  * @param String tipo di latte usato per fare il formaggio.
	**/
   public void updateTipoLatte(int idFormaggio, String tipoLatte) {
	   EntityManager entitymanager = inizializePersistance();
	   Formaggio Formaggio = entitymanager.find( Formaggio.class, idFormaggio);
	   if(Formaggio != null) {
		   Formaggio.setTipoLatte(tipoLatte);
		   entitymanager.getTransaction( ).commit( );
	   } 
	   closePersistance(entitymanager);
   }
   
   /** 
	  * Metodo che permette di aggiornare il caglio di un formaggio.
	  * @param identificativo del prodotto.
	  * @param String caglio, usato per il formaggio.
	**/
   public void updateCaglio(int idFormaggio, String caglio) {
	   EntityManager entitymanager = inizializePersistance();
	   Formaggio Formaggio = entitymanager.find( Formaggio.class, idFormaggio);
	   if(Formaggio != null) {
		   Formaggio.setCaglio(caglio);
		   entitymanager.getTransaction( ).commit( );
	   }
	   closePersistance(entitymanager); 
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata dal formaggio con l'id passato.
	  * @param identificativo del formaggio da ricercare.
	  * @return lista formata dal formaggio con id passato se presente.
	**/
   public List<Formaggio> findFormaggio(int id) {
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Formaggio f where f.codice = " + id);
	   List<Formaggio> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata da tutti i formaggi.
	  * @return la lista formata da tutti i formaggi
	**/
   public List<Formaggio> findFormaggio(){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Formaggio f");
	   List<Formaggio> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata dai formaggi con un determinato nome.
	  * @param nome di formaggio da ricercare.
	  * @return lista formata dai formaggi con il nome passato.
	**/
   public List<Formaggio> findByName(String nome){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Formaggio f where f.nome LIKE '" + nome + "'");
	   List<Formaggio> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
   
   /** 
	  * Metodo che permette di recuperare la lista formata dai formmaggi con prezzo maggiore di quello passato.
	  * @param prezzo da confrontare.
	  * @return lista formata dal formaggio con il prezzo maggiore di quello passato.
	**/
   public List<Formaggio> findByUpperPrice(int prezzo){
	   EntityManager entitymanager = inizializePersistance();
	   Query query = entitymanager.createQuery("Select f from Formaggio f where f.prezzo <= '" + prezzo + "'");
	   List<Formaggio> list = query.getResultList();
	   closePersistance(entitymanager);
	   return list;
   }
}
