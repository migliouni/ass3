package Exceptions;
/**
 * Classe che rappresenta eccezione per una posizione non valida, in particolare per una città nulla.
 **/
public class PosizioneNotValidException extends Exception {
 
 private static final long serialVersionUID = 1L;

 public PosizioneNotValidException() {
  super("La posizione non può avere citta null!");
 }

 public PosizioneNotValidException(String message) {
  super(message);
 }
}