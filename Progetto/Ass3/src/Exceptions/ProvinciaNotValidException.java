package Exceptions;
/**
 * Classe che rappresenta eccezione per la provincia di una posizione scorretta, in particolare se non composta da due lettere.
 **/
public class ProvinciaNotValidException extends Exception {
 
 private static final long serialVersionUID = 1L;

 public ProvinciaNotValidException() {
  super("La provincia deve essere composta da due lettere!");
 }

 public ProvinciaNotValidException(String message) {
  super(message);
 }
}
 

