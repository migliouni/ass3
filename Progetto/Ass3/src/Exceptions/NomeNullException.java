package Exceptions;
/**
 * Classe che rappresenta eccezione per un nome obbligatorio setta a null
 **/
public class NomeNullException extends Exception {
	 
	 private static final long serialVersionUID = 1L;

	 public NomeNullException() {
	  super("Il nome non può assumere valore null!");
	 }

	 public NomeNullException(String message) {
	  super(message);
	 }
}
