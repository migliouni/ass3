package Exceptions;
/**
 * Classe che rappresenta eccezione per un prezzo di un prodotto non valido, nel caso di prezzo negativo o nullo.
 **/
public class PrezzoNullNegativeException extends Exception {
 
 private static final long serialVersionUID = 1L;

 public PrezzoNullNegativeException() {
  super("Il prezzo di un prodotto non può essere vuoto o negativo!");
 }

 public PrezzoNullNegativeException(String message) {
  super(message);
 }
}






