package Exceptions;
/**
 * Classe che rappresenta eccezione per una posizione non valida, in particolare per un CAP non composto da 5 numeri.
 **/
public class CapNotValidException extends Exception {
 
 private static final long serialVersionUID = 1L;

 public CapNotValidException() {
  super("Il cap deve essere composto da 5 numeri interi!");
 }

 public CapNotValidException(String message) {
  super(message);
  
 }
}
 
