package Exceptions;
/**
 * Classe che rappresenta eccezione per una posizione non valida, per un indirizzo nullo.
 **/
public class IndirizzoNotValidException extends Exception {
	private static final long serialVersionUID = 1L;

	public IndirizzoNotValidException() {
		  super("L'indirizzo non può essere nullo");
		 }

		 public IndirizzoNotValidException(String message) {
		  super(message);
		  
		 }
}
