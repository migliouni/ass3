package Beans;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.InheritanceType;
import javax.persistence.DiscriminatorType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.JOINED )
@DiscriminatorColumn( name = "tipoProdotto", discriminatorType = DiscriminatorType.STRING )
/**
 * Classe che rappresenta un generico prodotto, classe minimale contenente setters e getters degli attributi, 
 * le classi che estendono Prodotto(Formaggio e Frutto) aggiungo attributi caratteristici. In questa classe vengono
 * rappresentati gli elementi comuni a tutti i prodotti.
 **/
public class Prodotto{
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO) 	
	  @Column(name = "codice")
	  /**
	   * Codice intero autogenerato univoco, per identificare un prodotto.
	   **/
	  protected int codice;
	  @Column(name = "prezzo", nullable = false)
	  /**
	   * double che descrive il prezzo del prodotto.
	   **/
	  protected double prezzo;
	  @Column(name = "nome", nullable = false)
	  /**
	   * Stringa, attributo per specificare il nome comune di un prodotto
	   **/
	  protected String nome;
	  
	  @ManyToMany(fetch=FetchType.LAZY)
	  @JoinTable(name = "PRODOTTODA", joinColumns = @JoinColumn(name = "codiceProdotto"),
	  inverseJoinColumns = @JoinColumn(name = "codiceProduttore"))
	  /**
	   * Insieme di produttori che produco il prodotto.
	   **/
	  protected Set<Produttore> produttori;
	 
	  @ManyToMany(fetch=FetchType.LAZY)
	  @JoinTable(name = "VENDUTODA", joinColumns = @JoinColumn(name = "codiceProdotto"),
	  inverseJoinColumns = @JoinColumn(name = "codiceVenditore"))
	  /**
	   * Insieme di venditori che vendono il prodotto.
	   **/
	  protected Set<Venditore> venditori;
	
	/**
	* Costruttore di default di un prodotto.
	**/
	public Prodotto() {
          super();
    }
	
	/** 
	* 	Metodo che permette di recuperare i produttori associati al prodotto.
	* 	@return Insieme di produttori associati al prodotto.
	**/  
	public Set<Produttore> getProduttori() {
		Set<Produttore> produttori;
		if (this.produttori!= null)
			produttori = new HashSet<Produttore>(this.produttori);
		else
			produttori = new HashSet<Produttore>();
		return produttori;
	}
	
	/** 
	* 	Metodo che permette di settare i produttori associati alla prodotto.
	* 	@param Insieme di produttori associati alla prodotto.
	**/
	public void setProduttori(Set<Produttore> produttori) {
		if (produttori!= null)
			this.produttori = new HashSet<Produttore>(produttori);
		else
			this.produttori = new HashSet<Produttore>();		
	}
	
	/** 
	* 	Metodo che permette di aggiungere un produttore associato al prodotto.
	* 	@param Produttore da aggiungere.
	**/
	public void addProduttore(Produttore produttore) {
	   if(produttore != null)
		   this.produttori.add(produttore);
    }
	/** 
	* 	Metodo che permette di rimuovere un produttore associato al prodotto.
	* 	@param Produttore da rimuovere.
	**/
	public void removeProduttore(Produttore produttore) {
		   if(produttore != null)
			   this.produttori.remove(produttore);
	}
	
	/** 
	* 	Metodo che permette di recuperare i venditori associati al prodotto.
	* 	@return Insieme di venditori associati al prodotto.
	**/ 
	public Set<Venditore> getVenditori() {
		Set<Venditore> venditori;
		if (this.venditori != null)
			venditori = new HashSet<Venditore>(this.venditori);
		else
			venditori = new HashSet<Venditore>();
		return venditori;
	}
	
	/** 
	* 	Metodo che permette di settare i venditori associati alla prodotto.
	* 	@param Insieme di venditori associati alla prodotto.
	**/
	public void setVenditori(Set<Venditore> venditori) {
		if (venditori != null)
			this.venditori = new HashSet<Venditore>(venditori);
		else
			this.venditori = new HashSet<Venditore>();
	}
	
	/** 
	* 	Metodo che permette di aggiugere un venditore associato al prodotto.
	* 	@param Venditore da rimuovere.
	**/
	public void addVenditore(Venditore venditore) {
		if(venditore != null)
		   this.venditori.add(venditore);
	}
	/** 
	* 	Metodo che permette di rimuovere un venditore associato al prodotto.
	* 	@param Venditore da rimuovere.
	**/
	public void removeVenditore(Venditore venditore) {
		if(venditore != null)
		   this.venditori.remove(venditore);
	}
	
	/** 
	* 	Metodo che permette di recuperare il codice identificativo del prodotto.
	* 	@return intero univico per identificare il prodotto.
	**/ 
	public int getCodice() {
  		return codice;
  	}
  	
	/** 
	* 	Metodo che permette di settare il codice identificativo del prodotto.
	* 	@param Codice intero da settare.
	**/ 
    public void setCodice(int codice) {
  		this.codice = codice;
  	}
  	
    /** 
	* 	Metodo che permette di recuperare il prezzo del prodotto.
	* 	@return double prezzo del prodotto.
	**/ 
    public double getPrezzo() {
  		return prezzo;
  	}
  	
    /** 
	* 	Metodo che permette di settare il prezzo da associare al prodotto.
	* 	@param double prezzo del prodotto.
	**/
    public void setPrezzo(double prezzo) {
  		this.prezzo = prezzo;
  	}

    /** 
   	* 	Metodo che permette di recuperare il nome del prodotto.
   	* 	@return Stringa del nome comune del prodotto.
   	**/ 
    public String getNome() {
  		return nome;
  	}

    /** 
 	* 	Metodo che permette di settare il nome da associare al prodotto.
 	* 	@param Stringa con il nome del prodotto.
 	**/
    public void setNome(String nome) {
  		this.nome = nome;
  	}
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codice;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		long temp;
		temp = Double.doubleToLongBits(prezzo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((produttori == null) ? 0 : produttori.hashCode());
		result = prime * result + ((venditori == null) ? 0 : venditori.hashCode());
		return result;
	}

    /** 
	* 	Override del metodo equals per determinare se due prodotti sono uguali.
    * 	@return valore booleana che stabilisce l'uguaglianza dei due prodotti.
    **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prodotto other = (Prodotto) obj;
		if (codice != other.codice)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (Double.doubleToLongBits(prezzo) != Double.doubleToLongBits(other.prezzo))
			return false;
		if (produttori == null) {
			if (other.produttori != null)
				return false;
		} else if (!produttori.equals(other.produttori))
			return false;
		if (venditori == null) {
			if (other.venditori != null)
				return false;
		} else {
			if(venditori.size() != other.getVenditori().size())
				return false;
			boolean trovato = false;
			for(Venditore venditore: venditori) {
				trovato = false;
				for(Venditore other_venditore: other.getVenditori()) {
					if(venditore.getCodiceVenditore() == other_venditore.getCodiceVenditore())
						trovato = true;
				}
				if(!trovato) return false;
			}
		}
		return true;
	}
	
}


   
	
   
   
   
