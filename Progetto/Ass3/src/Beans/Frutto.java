package Beans;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="codice")
/**
 * Classe che rappresenta un prodotto di tipo Frutto, classe minimale contenente setters e getters degli attributi, equals e toString.
 * Frutto estende Prodotto aggiungendo attributi caratteristici, come stagione, colore, varietà.
 **/
public class Frutto extends Prodotto  {

  @Column(name = "stagione")
  /**
   * Stringa per rappresentare la stagione in cui viene maggiormente prodotto il frutto.
   **/
  private String stagione;
  
  @Column(name = "colore")
  private String colore;
  /**
   * Stringa che rappresenta il colore del frutto.
   **/
  
  @Column(name = "varieta")
  private String varieta;
  /**
   * Stringa che rappresenta la varietà del frutto (per la mela ad esempio Fuji, Melinda ecc)
   **/
  
  /**
  * Costruttore di default di un frutto.
  **/
  public Frutto() {
	  super();
  }
  
  /** 
  * Metodo che permette di recuperare la stagione del frutto.
  * @return Stringa che rappresenta la stagione del frutto.
  **/ 
  public String getStagione() {
	  return stagione;
  }
  
  /** 
  * Metodo che permette di settare la stagione del frutto.
  *	@param Stringa che rappresenta la stagione del frutto da settare.
  **/
  public void setStagione(String stagione) {
	  this.stagione = stagione;
  }
  
  /** 
   * Metodo che permette di recuperare il colore del frutto.
   * @return Stringa che rappresenta il colore del frutto.
   **/ 
  public String getColore() {
	  return colore;
  }
  
  /** 
   * Metodo che permette di settare il colore del frutto.
   * @param Stringa che rappresenta il colore del frutto da settare.
   **/
  public void setColore(String colore) {
	  this.colore = colore;
  }
  
  /** 
  * Metodo che permette di recuperare la varietà(nome specifico) del frutto.
  * @return Stringa che rappresenta la varietà del frutto.
  **/ 
  public String getVarieta() {
	  return varieta;
  }
  
  /** 
   * Metodo che permette di settare la varietà del frutto.
   * @param Stringa che rappresenta la varietà del frutto da settare.
   **/
  public void setVarieta(String varieta) {
	  this.varieta = varieta;
  }
  
  /** 
  * Override del metodo toString di default, ritorna i valori del frutto in una singola stringa.
  *	@return Stringa che descrive il frutto, con tutti i suoi campi, anche quelli ereditati da prodotto.
  **/
  @Override
  public String toString() {
	  return "Frutto [codice=" + codice + ", prezzo=" + prezzo + ", nome=" + nome + ", stagione=" + stagione
  				+ ", colore=" + colore + ", varieta=" + varieta + "]";
  }
  
  @Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((colore == null) ? 0 : colore.hashCode());
		result = prime * result + ((stagione == null) ? 0 : stagione.hashCode());
		result = prime * result + ((varieta == null) ? 0 : varieta.hashCode());
		return result;
	}

  	/** 
	* Override del metodo equals per determinare se due frutti sono uguali.
	* @return valore booleana che stabilisce l'uguaglianza dei due frutti.
  	**/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frutto other = (Frutto) obj;
		if (colore == null) {
			if (other.colore != null)
				return false;
		} else if (!colore.equals(other.colore))
			return false;
		if (stagione == null) {
			if (other.stagione != null)
				return false;
		} else if (!stagione.equals(other.stagione))
			return false;
		if (varieta == null) {
			if (other.varieta != null)
				return false;
		} else if (!varieta.equals(other.varieta))
			return false;
		return true;
	} 
}
