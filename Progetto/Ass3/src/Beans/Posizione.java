package Beans;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table; 
/**
 * Classe che rappresenta una posizione, classe minimale contenente setters e getters degli attributi, metodo toString e equals.
 **/
@Entity
@Table(name = "POSIZIONE")
public class Posizione {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "codicePosizione")
  /**
   * Codice univico, autoincrementate per identificare una specifica posizione.
   **/
  private int codicePosizione;
  
  @Column(name = "indirizzo", nullable = false)
  /**
   * Indirizzo della poszione, stringa composta da via e numero, campo non settabile a null.
   **/
  private String indirizzo;
  @Column(name = "citta", nullable = false)
  
  /**
   * Stringa che rappresenta la città della posizione, campo non settabile a null.
   **/
  private String citta;
  @Column(name = "cap")
  /**
  * Intero di 5 cifre che rappresenta il CAP(codice di avviamento postale) della posizione.
  **/
  private int cap;
  @Column(name = "regione")
  
  /**
   * Stringa che rappresenta la regione della posizione.
   **/
  private String regione;
  
  @Column(name = "provincia")
  /**
   * Stringa composta da due caratteri che rappresentano la provincia della posizione.
   **/
  private String provincia;
  
   /** 
    * 	Metodo che permette di recuperare l'indirizzo associato alla posizione.
    * 	@return indirizzo della posizione.
    **/
    public String getIndirizzo() {
	   return indirizzo;
    }

   /** 
    * 	Metodo che permette di settare l'indirizzo associato alla posizione.
    * 	@param Stringa dell'inidirizzo(via e numero) da associare alla posizione.
    **/
    public void setIndirizzo(String indirizzo) {
	   this.indirizzo = indirizzo;
    }
   
   /** 
    * 	Metodo che permette di recuperare l'id associato alla posizione.
    * 	@return il codice della posizione.
    **/
    public int getCodicePos() {
	   return codicePosizione;
    }
   
   /** 
    * 	Metodo che permette di recuperare la città associata alla posizione.
    * 	@return la città della posizione.
    **/
    public String getCitta() {
		return citta;
    }
	
	/** 
	* 	Metodo che permette di settare la città alla posizione.
	*   @param Stringa della città da associare alla posizione.
	**/
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	/** 
	* 	Metodo che permette di recuperare il CAP associato alla posizione.
    * 	@return il CAP della posizione.
    **/
	public int getCap() {
		return cap;
	}
	
	/** 
	* 	Metodo che permette di settare il CAP alla posizione.
	*   @param Cap di tipo Intero da associare alla posizione.
	**/
	public void setCap(int cap) {
		this.cap = cap;
	}
	
	/** 
	* 	Metodo che permette di settare l'identificativo della posizione.
	*   @param codice identificativo della posizione.
	**/
	public void setCodicePos(int codicePos) {
		this.codicePosizione = codicePos;
		
	}
	
	/** 
	* 	Metodo che permette di recuperare la regione associata alla posizione.
    * 	@return la regione della posizione.
    **/
	public String getRegione() {
		return regione;
	}
	
	/** 
	* 	Metodo che permette di settare la regione della posizione.
	*   @param Stringa della città da associare alla posizione.
	**/
	public void setRegione(String regione) {
		this.regione = regione;
	}
	
	/** 
	* 	Metodo che permette di recuperare la provincia associata alla posizione.
    * 	@return la provincia della posizione.
    **/
	public String getProvincia() {
		return provincia;
	}
	
	/** 
	* 	Metodo che permette di settare la provincia alla posizione.
	*   @param Stringa da settare la provincia da associare alla posizione.
	**/
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	/** 
	* 	Costruttore di default per una posizione.
    * 	
    **/
	public Posizione() {
	      super();
	}
	
	/** 
	* 	Override del metodo toString di default, ritorna i valori della posizione sotto forma di un'unica stringa.
    * 	@return Stringa che descrive una posizione, con tutti i suoi campi.
    **/
	@Override
	public String toString() {
		return "Posizione [codicePosizione=" + codicePosizione + ", citta=" + citta + ", cap=" + cap + ", regione="
				+ regione + ", provincia=" + provincia + "]";
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cap;
		result = prime * result + ((citta == null) ? 0 : citta.hashCode());
		result = prime * result + codicePosizione;
		result = prime * result + ((indirizzo == null) ? 0 : indirizzo.hashCode());
		result = prime * result + ((provincia == null) ? 0 : provincia.hashCode());
		result = prime * result + ((regione == null) ? 0 : regione.hashCode());
		return result;
	}
	
	/** 
	* 	Override del metodo equals per determinare se due posizioni sono uguali.
    * 	@return valore booleana che stabilisce l'uguaglianza delle posizioni.
    **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Posizione other = (Posizione) obj;
		if (cap != other.cap)
			return false;
		if (citta == null) {
			if (other.citta != null)
				return false;
		} else if (!citta.equals(other.citta))
			return false;
		if (codicePosizione != other.codicePosizione)
			return false;
		if (indirizzo == null) {
			if (other.indirizzo != null)
				return false;
		} else if (!indirizzo.equals(other.indirizzo))
			return false;
		if (provincia == null) {
			if (other.provincia != null)
				return false;
		} else if (!provincia.equals(other.provincia))
			return false;
		if (regione == null) {
			if (other.regione != null)
				return false;
		} else if (!regione.equals(other.regione))
			return false;
		return true;
	}	
}
