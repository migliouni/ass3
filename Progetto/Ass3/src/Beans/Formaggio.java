package Beans;
import javax.persistence.Column; 
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="codice")
/**
 * Classe che rappresenta un prodotto di tipo Formaggio, classe minimale contenente setters e getters degli attributi, equals e toString.
 * Formaggio estende Prodotto aggiungo attributi caratteristici, come stagionatura, tipo di latte, caglio.
 **/
public class Formaggio extends Prodotto{
 
  @Column(name = "stagionatura") 
  /**
   * Numero intero per specificare il numero di mesi, che il formaggio ha stagionato.
   **/
  private int stagionatura;
  
  @Column(name = "tipoLatte")
  /**
   * Stringa per rappresentare il tipo di latte utilizzato per creare il formaggio (Bovino, Ovino, ecc).
   **/
  private String tipoLatte;
  
  @Column(name = "caglio")
  /**
   * Stringa per rappresentare la presenza o meno del caglio nel formaggio.
   **/
  private String caglio;
  
  	/**
	* Costruttore di default di un formaggio.
	**/
  	public Formaggio() {
      super();
    }
	
  	/** 
	* 	Metodo che permette di recuperare la stagionatura del formaggio.
	* 	@return intero che rappresenta il numero di mesi che ha stagionato il formaggio.
	**/ 
	public int getStagionatura() {
		return stagionatura;
	}
	
	/** 
	* 	Metodo che permette di settare i mesi di stagionatura del formaggio.
	* 	@param intero, numero di mesi di stagionatura.
	**/
	public void setStagionatura(int stagionatura) {
		this.stagionatura = stagionatura;
	}
	
  	/** 
	* 	Metodo che permette di recuperare il tipo di latte del formaggio.
	* 	@return stringa che rappresenta il tipo di latte del formaggio.
	**/ 
	public String getTipoLatte() {
		return tipoLatte;
	}
	
	/** 
	* 	Metodo che permette di settare il tipo di latte del formaggio.
	* 	@param stringa che specifica il tipo di latte del formaggio.
	**/
	public void setTipoLatte(String tipoLatte) {
		this.tipoLatte = tipoLatte;
	}
	
	/** 
	* 	Metodo che permette di recuperare il caglio del prodotto.
	* 	@return stringa rappresenta il caglio del formaggio.
	**/ 
	public String getCaglio() {
		return caglio;
	}
	
	/** 
	* 	Metodo che permette di settare il caglio del formaggio.
	* 	@param stringa che specifica il tipo di caglio.
	**/
	public void setCaglio(String caglio) {
		this.caglio = caglio;
	}
	
	/** 
	* 	Override del metodo toString di default, ritorna i valori del formaggio sotto forma di un'unica stringa.
    * 	@return .Stringa che descrive il formaggio, con tutti i suoi campi, anche quelli ereditati da prodotto.
    **/
	@Override
	public String toString() {
		return "Formaggio [codice=" + codice + ", prezzo=" + prezzo + ", nome=" + nome + ", stagionatura="
				+ stagionatura + ", tipoLatte=" + tipoLatte + ", caglio=" + caglio + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((caglio == null) ? 0 : caglio.hashCode());
		result = prime * result + stagionatura;
		result = prime * result + ((tipoLatte == null) ? 0 : tipoLatte.hashCode());
		return result;
	}

	/** 
	* 	Override del metodo equals per determinare se due formaggi sono uguali.
    * 	@return valore booleana che stabilisce l'uguaglianza dei due formaggi.
    **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Formaggio other = (Formaggio) obj;
		if (caglio == null) {
			if (other.caglio != null)
				return false;
		} else if (!caglio.equals(other.caglio))
			return false;
		if (stagionatura != other.stagionatura)
			return false;
		if (tipoLatte == null) {
			if (other.tipoLatte != null)
				return false;
		} else if (!tipoLatte.equals(other.tipoLatte))
			return false;
		return true;
	}
}
