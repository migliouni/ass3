package Beans;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table; 
@Entity
@Table(name = "PRODUTTORE")
/**
 * Classe che rappresenta un produttore classe minimale contenente setters e getters degli attributi, equals e toString.
 **/
public class Produttore {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "codiceAzienda")
  /**
   * Codice intero autogenerato, identifica un produttore.
   **/
  private int codiceAzienda;
  
  @Column(name = "nome" , nullable = false)
  /**
   * Stringa per rappresentare il nome del produttore.
   **/
  private String nome;
  
  /**
   * Intero per rappresentare la partita IVA. del produttore.
   **/
  @Column(name = "partitaIva")
  private int partitaIva;
  
  /**
   * Stringa per rappresentare il tipo di produzione del produttore.
   **/
  @Column(name = "tipoProduzione")
  private String tipoProduzione;
  
  /**
   * Posizione, rappresenta la posizione del produttore.
   **/
  @OneToOne
  @JoinColumn(name="codicePosizione")
  private Posizione codicePosizione;
    
  	/**
  	* Costruttore di default di un frutto.
  	**/
  	public Produttore() {
      	super();
  	}
  	
  	/** 
   	* Metodo che permette di accedere al codice identificativo del produttore.
   	* @return identificativo del produttore.
    **/ 
	public int getCodiceAzienda() {
		return codiceAzienda;
	}
	
	/** 
   	* Metodo che permette di recuperare il nome del produttore.
   	* @return Stringa contenente il nome del produttore.
    **/ 
	public String getNome() {
		return nome;
	}
	
	/** 
  	* Metodo che permette di settare il nome al produttore.
  	* @param Stringa con il nome del produttore.
  	**/
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/** 
   	* Metodo che permette di settare il codice identificativo del produttore.
   	* @param codice identificativo del produttore.
    **/ 
	public void setCodiceAzienda(int codiceAzienda) {
		this.codiceAzienda = codiceAzienda;
	}
	
	/** 
   	* Metodo che permette di recuperare la partita IVA del produttore.
   	* @return intero contenente la partita IVA. del produttore
    **/ 
	public int getPartitaIva() {
		return partitaIva;
	}
	
	/** 
  	* Metodo che permette di settare la partita IVA al produttore.
  	* @param Intero che rappresenta la partita IVA.
  	**/
	public void setPartitaIva(int partitaIva) {
		this.partitaIva = partitaIva;
	}
	
	/** 
   	* Metodo che permette di recuperare il tipo di produzione del produttore.
   	* @return Stringa contenente la descrizione del tipo di produzione del produttore.
    **/ 
	public String getTipoProduzione() {
		return tipoProduzione;
	}
	
	/** 
  	* Metodo che permette di settare il tipo di produzione del produttore.
  	* @param Stringa contenente il tipo di produzione del produttore.
  	**/
	public void setTipoProduzione(String tipoProduzione) {
		this.tipoProduzione = tipoProduzione;
	}
	
	/** 
   	* Metodo che permette di recuperare la posizione del produttore.
   	* @return Oggetto Posizione che descrive la posizione del produttore.
    **/ 
	public Posizione getCodicePosizione() {
		Posizione posizione = new Posizione();
		if (this.codicePosizione != null){
			posizione.setCap(this.codicePosizione.getCap());
			posizione.setCitta(this.codicePosizione.getCitta());
			posizione.setCodicePos(this.codicePosizione.getCodicePos());
			posizione.setIndirizzo(this.codicePosizione.getIndirizzo());
			posizione.setProvincia(this.codicePosizione.getProvincia());
			posizione.setRegione(this.codicePosizione.getRegione());
		} 
		else
			posizione = null;
		return posizione;
	}
	
	/**
  	* Metodo che permette di settare la posizione del produttore.
  	* @param Oggetto Posizione che descrive la posizione del produttore.
  	**/
	public void setCodicePosizione(Posizione posizione) {
		this.codicePosizione = new Posizione();
		if(posizione != null) {
			this.codicePosizione.setCap(posizione.getCap());
			this.codicePosizione.setCitta(posizione.getCitta());
			this.codicePosizione.setCodicePos(posizione.getCodicePos());
			this.codicePosizione.setIndirizzo(posizione.getIndirizzo());
			this.codicePosizione.setProvincia(posizione.getProvincia());
			this.codicePosizione.setRegione(posizione.getRegione());
		}
		this.codicePosizione = posizione;
	}
	
	/** 
	* Override del metodo toString di default, ritorna i valori del produttore in una singola stringa.
	* @return Stringa che descrive il produttore.
	**/
	@Override
	public String toString() {
		return "Produttore [codiceAzienda=" + codiceAzienda + ", nome=" + nome + ", partitaIva=" + partitaIva
				+ ", tipoProduzione=" + tipoProduzione + ", codicePosizione=" + codicePosizione + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codiceAzienda;
		result = prime * result + ((codicePosizione == null) ? 0 : codicePosizione.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + partitaIva;
		result = prime * result + ((tipoProduzione == null) ? 0 : tipoProduzione.hashCode());
		return result;
	}
	
	/** 
	* 	Override del metodo equals per determinare se due produttori sono uguali.
    * 	@return valore booleana che stabilisce l'uguaglianza dei due produttori.
    **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produttore other = (Produttore) obj;
		if (codiceAzienda != other.codiceAzienda)
			return false;
		if (codicePosizione == null) {
			if (other.codicePosizione != null)
				return false;
		} else if (!codicePosizione.equals(other.codicePosizione))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (partitaIva != other.partitaIva)
			return false;
		if (tipoProduzione == null) {
			if (other.tipoProduzione != null)
				return false;
		} else if (!tipoProduzione.equals(other.tipoProduzione))
			return false;
		return true;
	}
}
