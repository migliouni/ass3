package Beans;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity
@Table(name = "VENDITORE")
/**
  * Classe che rappresenta un venditore, classe minimale contenente setters e getters degli attributi, equals e toString.
  **/
public class Venditore{
	@Id
  	@GeneratedValue(strategy = GenerationType.AUTO)
  	@Column(name = "codiceVenditore")
	/**
	 * Codice intero autogenerato, identifica un venditore.
	 **/
  	private int codiceVenditore;
  	
  	@Column(name = "nome", nullable = false)
  	/**
  	 * Stringa per rappresentare il nome del venditore.
     **/
  	private String nome;
  	
  	@Column(name = "tipoVenditore")
  	/**
     * Stringa per rappresentare il tipo di venditore.
     **/
  	private String tipoVenditore;
  
  	@ManyToMany(fetch=FetchType.LAZY)
  	@JoinTable(name = "POSIZIONEVEN", joinColumns = @JoinColumn(name = "codiceVenditore"),
  	inverseJoinColumns = @JoinColumn(name = "codicePosizione"))
  	/**
     * Insieme di posizioni, rappresentano le località dove è presente il venditore.
     **/
  	private Set<Posizione> posizioni;
  
  	@ManyToMany(fetch=FetchType.LAZY)
  	@JoinTable(name = "SOCIO", joinColumns = @JoinColumn(name = "codiceSocio1"),
  	inverseJoinColumns = @JoinColumn(name = "codiceSocio2"))
  	/**
     * Insieme di Venditori, rappresentano i soci di minoranza del venditore.
     **/
  	private Set<Venditore> soci;
  	
  	/** 
   	* Metodo che permette di recuperare i soci di minoranza del venditore.
   	* @return Insieme di venditori soci del venditore.
    **/ 
  	public Set<Venditore> getSoci() {
  		Set<Venditore> soci;
  		if(this.soci != null)
  			soci = new HashSet<Venditore>(this.soci);
  		else
  			soci = null;
  		return soci;
  	}
  	
  	/** 
  	* Metodo che permette di settare un insieme di venditori come soci di minoranza.
  	* @param Insieme di soci di minoranza.
  	**/
  	public void setSoci(Set<Venditore> soci) {
  		if(soci != null)
  			this.soci = new HashSet<Venditore>(soci);
  		else
  			this.soci = new HashSet<Venditore>();
  	}
  	
  	/** 
  	* Metodo che permette di aggiungere un socio di minoranza.
  	* @param Socio di minoranza da aggiungere.
  	**/
  	public void addSocio(Venditore socio) {
  		if(socio != null)
  			this.soci.add(socio);
  	}
  	
  	/** 
   	* Metodo che permette di rimuovere un socio.
   	* @param Socio di minoranza da rimuovere.
    **/ 
  	public void removeSocio(Venditore socio) {
  		if(socio != null)
  			this.soci.remove(socio);
  	}
  	
  	/**
  	* Costruttore di default di un frutto.
  	**/
	public Venditore() {
		super();
  	}
	
	/** 
   	* Metodo che permette di recuperare il codice identificativo del Venditore.
   	* @return codice intero del venditore.
    **/ 
    public int getCodiceVenditore() {
		return codiceVenditore;
	}
	
    /** 
   	* Metodo che permette di recuperare il nome del venditore.
   	* @return Stringa col nome del venditore.
    **/ 
	public String getNome() {
		return nome;
	}
	
	/** 
  	* Metodo che permette di settare il nome del venditore.
  	* @param Stringa col nome del venditore.
  	**/
	public void setNome(String nome) {
		this.nome = nome;
	}
	/** 
   	* Metodo che permette di recuperare il tipo di venditore.
   	* @return Stringa contenente il tipo di venditore.
    **/ 
	public String getTipoVenditore() {
		return tipoVenditore;
	}
	
	/** 
  	* Metodo che permette di settare il tipo di venditore.
  	* @param Stringa contenente il tipo di venditore.
  	**/
	public void setTipoVenditore(String tipoVenditore) {
		this.tipoVenditore = tipoVenditore;
	}
	
	/** 
   	* Metodo che permette di recuperare le posizioni del venditore.
   	* @return Insieme di Posizioni che descrivono le localilità del venditore.
    **/ 
	public Set<Posizione> getPosizioni() {
		  Set<Posizione> posizioni;
		  if (this.posizioni!= null)
				posizioni = new HashSet<Posizione>(this.posizioni);
			else
				posizioni = new HashSet<Posizione>();
		  return posizioni;
	}

  	/** 
  	* Metodo che permette di settare le posizioni del venditore.
  	* @param insieme di Posizione che descrivono le localilità del venditore.
  	**/
	public void setPosizioni(Set<Posizione> posizioni) {
		if (posizioni!= null)
			this.posizioni = new HashSet<Posizione>(posizioni);
		else
			this.posizioni = new HashSet<Posizione>();
	}
	
	/** 
  	* Metodo che permette di aggiungere una poszione ad un venditore.
  	* @param Oggetto Posizione da aggiungere.
  	**/
	public void addPosizione(Posizione posizione) {
		if(posizione != null)
			this.posizioni.add(posizione);
	}
    
	/** 
  	* Metodo che permette di rimuovere una poszione da un venditore.
  	* @param Oggetto Posizione da rimuovere.
  	**/
	public void removePosizione(Posizione posizione) {
		if(posizione != null)
			this.posizioni.remove(posizione);
	}
	
	/** 
	* Override del metodo toString di default, ritorna i valori del venditore in una singola stringa.
	* @return Stringa che descrive il venditore.
	**/
	@Override
	public String toString() {
		return "Venditore [codiceVenditore=" + codiceVenditore + ", nome=" + nome + ", tipoVenditore=" + tipoVenditore
				+ ", posizioni=" + posizioni + ", soci=" + soci + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codiceVenditore;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((posizioni == null) ? 0 : posizioni.hashCode());
		//result = prime * result + ((prodotti == null) ? 0 : prodotti.hashCode());
		result = prime * result + ((soci == null) ? 0 : soci.hashCode());
		result = prime * result + ((tipoVenditore == null) ? 0 : tipoVenditore.hashCode());
		return result;
	}
	
	/** 
	* 	Override del metodo equals per determinare se due venditori sono uguali.
    * 	@return valore booleana che stabilisce l'uguaglianza dei due venditori.
    **/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venditore other = (Venditore) obj;
		if (codiceVenditore != other.codiceVenditore)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (posizioni == null) {
			if (other.posizioni != null)
				return false;
		} else { 
			if(posizioni.size() != other.getPosizioni().size())
				return false;
			boolean trovato = false;
			for(Posizione posizione: posizioni) {
				trovato = false;
				for(Posizione other_posizione: other.getPosizioni()) {
					if(posizione.getCodicePos() == other_posizione.getCodicePos())
						trovato = true;
				}
				if(!trovato) return false;
			}
		}
		if (soci == null) {
			if (other.soci != null)
				return false;
		} else { 
			if(soci.size() != other.getSoci().size())
				return false;
			boolean trovato = false;
			for(Venditore venditore: soci) {
				trovato = false;
				for(Venditore other_venditore: other.getSoci()) {
					if(venditore.getCodiceVenditore() == other_venditore.getCodiceVenditore())
						trovato = true;
				}
				if(!trovato) return false;
			}
		}
		if (tipoVenditore == null) {
			if (other.tipoVenditore != null)
				return false;
		} else if (!tipoVenditore.equals(other.tipoVenditore))
			return false;
		return true;
	}
}
