var searchData=
[
  ['updatecaglio',['updateCaglio',['../class_controllers_1_1_controller_formaggio.html#ad91d1adf3e9e4f0e75a6d1c4d6fd4c71',1,'Controllers::ControllerFormaggio']]],
  ['updatecap',['updateCap',['../class_controllers_1_1_controller_posizione.html#abc859b2041c750f97ffd54f8e5be972f',1,'Controllers::ControllerPosizione']]],
  ['updatecitta',['updateCitta',['../class_controllers_1_1_controller_posizione.html#a729328585b0befa8f645fbcbd09b12a9',1,'Controllers::ControllerPosizione']]],
  ['updatecolore',['updateColore',['../class_controllers_1_1_controller_frutto.html#a3542d10a6a2ea393923a54fe16c47bd0',1,'Controllers::ControllerFrutto']]],
  ['updateformaggio',['updateFormaggio',['../class_controllers_1_1_controller_formaggio.html#a6a9642ee0bcdc7b8a2ffb6f551ed4c5e',1,'Controllers::ControllerFormaggio']]],
  ['updatefrutto',['updateFrutto',['../class_controllers_1_1_controller_frutto.html#af225ca1d74b2a11b9eeef27abb6dfb87',1,'Controllers::ControllerFrutto']]],
  ['updateindirizzo',['updateIndirizzo',['../class_controllers_1_1_controller_posizione.html#a0aca1385d947cedbbf16a1cc9f6b5efa',1,'Controllers::ControllerPosizione']]],
  ['updatenome',['updateNome',['../class_controllers_1_1_controller_prodotto.html#af2dffa9469a98636c4bed8cefa01beb1',1,'Controllers.ControllerProdotto.updateNome()'],['../class_controllers_1_1_controller_produttore.html#a0d29f4d6f4ffaf660708bc7d8d6d5624',1,'Controllers.ControllerProduttore.updateNome()'],['../class_controllers_1_1_controller_venditore.html#a00d7ff38d13c4d0af82cb129c36fd9c3',1,'Controllers.ControllerVenditore.updateNome()']]],
  ['updatepiva',['updatePIva',['../class_controllers_1_1_controller_produttore.html#ae357d1d8c294123323059d3818f15080',1,'Controllers::ControllerProduttore']]],
  ['updateposizione',['updatePosizione',['../class_controllers_1_1_controller_posizione.html#a158a6731aa0a9e0fd12701b9cdcd0e92',1,'Controllers.ControllerPosizione.updatePosizione()'],['../class_controllers_1_1_controller_produttore.html#aabeb1f227864d2612084b2cc926763f5',1,'Controllers.ControllerProduttore.updatePosizione()']]],
  ['updateprezzo',['updatePrezzo',['../class_controllers_1_1_controller_prodotto.html#a8dc8071bcd9485006bc6f2a6cbe80072',1,'Controllers::ControllerProdotto']]],
  ['updateproduttore',['updateProduttore',['../class_controllers_1_1_controller_produttore.html#a455e11c8d312a89cbf9ee974d0310c6e',1,'Controllers::ControllerProduttore']]],
  ['updateprovincia',['updateProvincia',['../class_controllers_1_1_controller_posizione.html#aefc89487ca011dbaaf8ad8daa5a5b64c',1,'Controllers::ControllerPosizione']]],
  ['updateregione',['updateRegione',['../class_controllers_1_1_controller_posizione.html#adee74568f5ae9396a86dc9a3ac0fc2af',1,'Controllers::ControllerPosizione']]],
  ['updatestagionatura',['updateStagionatura',['../class_controllers_1_1_controller_formaggio.html#aa380c46b31f63b110aeb4d57440c7775',1,'Controllers::ControllerFormaggio']]],
  ['updatestagione',['updateStagione',['../class_controllers_1_1_controller_frutto.html#adb561a9137c8f5e4b2df0ceb044c02d8',1,'Controllers::ControllerFrutto']]],
  ['updatetipolatte',['updateTipoLatte',['../class_controllers_1_1_controller_formaggio.html#abfe4c6eb46bb26a9e9a6aeced6787f6c',1,'Controllers::ControllerFormaggio']]],
  ['updatetipoproduzione',['updateTipoProduzione',['../class_controllers_1_1_controller_produttore.html#a70e6e5b70d34c0de392a7df9f0d6ce07',1,'Controllers::ControllerProduttore']]],
  ['updatetipovenditore',['updateTipoVenditore',['../class_controllers_1_1_controller_venditore.html#a17b0a1b3a38bfd9abac6dc6c277ddb3f',1,'Controllers::ControllerVenditore']]],
  ['updatevarieta',['updateVarieta',['../class_controllers_1_1_controller_frutto.html#a7f05c3c5d72e45ec78158e1d612ccf90',1,'Controllers::ControllerFrutto']]],
  ['updatevenditore',['updateVenditore',['../class_controllers_1_1_controller_venditore.html#af9947d26ec8b05072823daf00f7d53d8',1,'Controllers::ControllerVenditore']]]
];
