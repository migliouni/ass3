# ass3
I. Prerequisiti
--------------

Per poter eseguire il programma bisogna disporre dei seguenti componenti<br />
<br />
1# Java: scaricare e installare la versione di Java JRE corretta per il proprio sistema<br />
2# JDK: scaricare e installare Java SE 2 JDK 1.5 o superiore<br />
3# Eclipse: scaricare Eclipse IDE e scegliere JavaIDE per JavaEE<br />

II. Importare Progetto
----------------------

L'assignement è stato realizzato con un progetto JPA e EclipseLink, di seguito è spiegato come importarlo a partire da un nuovo progetto Eclipse<br />
<br />
1# Creare un nuovo progetto JPA usando File -> New -> JPA Project<br />
2# Dare un nome a scelta al progetto e selezionare la versione di JRE tramite la tendina di "Turget Runtime", e premere "next"<br />
3# Nella sezione delle librerie utente scaricare la libreria EclipseLink se non si dispone già di essa <br />
4# A questo punto bisogna aggiungere MySql connector al progetto, l'archivio è gia presente alla posizione Progetto/Ass3/libraries/mysql-connector-java-5.1.47/mysql-connector-java-5.1.47/mysql-connector-java-5.1.47.jar: raggiungere le proprietà del progetto, cliccare su "Java Build Path" e aggiungere una libreria esterna tramite "Add External Jars" <br />
5# Importare la cartella Progetto/Ass3 della repository cliccando con il tasto destro sul progetto appena creato e poi premendo "Import", nella sezione "General" scegliere "File System", per il campo "From Directory" selezionare la cartella Progetto/Ass3 della repository, invece per il campo "Into folder" scegliere il progetto JPA appena creato <br /> 
6# Nel file persistance.xml sono specificate le credenziale per accedere al server MySql locale(utente: root, password: root)

III. Testare il progetto
------------------------

Per testa il progetto abbiamo usufruito della libreria JUnit 5 presente nel pacchetto Eclipse IDE per Java EE<br />
<br />
1# JUnit può essere aggiunto al progetto cliccando con il tasto destro su di esso e selezionando Build Path -> Add Libraries... e poi selezionando "JUnit" e cliccando "next"<br />
2# Per eseguire un test raggiungere la cartella src/Tests, a questo punto si scelga un test a piacimento e premere il pulsante Run dalla barra degli strumenti<br />

